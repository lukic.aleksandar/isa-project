package dao;

import java.util.List;

import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import model.Reservation;
import model.ReservationPK;

@Stateless( name = "ReservationDao")
@Local(ReservationDaoLocal.class)
@Remote(ReservationDao.class)
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class ReservationBeanDao 
extends GenericDaoBean<Reservation, ReservationPK> 
implements ReservationDao {

	public List<Reservation> findReservationsForUser(int userId) {
		String query = "Select r from Reservation r where r.id.usrId = "+userId;
		return findBy(query);
	}

}
