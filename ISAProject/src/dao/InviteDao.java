package dao;

import java.util.List;

import model.Invite;
import model.InvitePK;
import model.User;

public interface InviteDao extends GenericDao<Invite, InvitePK>{
		
	public List<Invite> findInvitationsForUser(int usrId);
	public Invite findByToken(String token);
	public List<User> findFriendsInInvitation(Invite inv);
}
