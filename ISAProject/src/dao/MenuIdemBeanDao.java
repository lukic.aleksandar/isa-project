package dao;

import java.util.List;

import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import model.Menuitem;
import model.MenuitemPK;

@Stateless(name = "MenuItemDao")
@Local(MenuItemDaoLocal.class)
@Remote(MenuItemDao.class)
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class MenuIdemBeanDao extends GenericDaoBean<Menuitem, MenuitemPK> implements MenuItemDao{

	public List<Menuitem> findAllForRestaurant(int rsID) {
		// TODO Auto-generated method stub
		String query = "Select mi from Menuitem mi where mi.id.rsId = " + rsID;
		return findBy(query);
	}

}
