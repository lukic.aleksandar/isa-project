package dao;

import java.util.List;

import model.User;

public interface UserDao extends GenericDao<User, Integer>{

	public List<User> findFriendsOfUser(int usrId);
	public List<User> findFriendOfUser(int userId, int friendId);
	public User findByToken(String token);
	public User findByEmail(String email);
	public List<User> findByNameAndSurname(int id, String[] s);
}
