package dao;

import java.util.List;

import model.Reservation;
import model.ReservationPK;

public interface ReservationDao extends GenericDao<Reservation, ReservationPK>{
	
	public List<Reservation> findReservationsForUser(int userId);
}
