package dao;

import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import model.Restaurant;

@Stateless( name = "RestaurantBean")
@Local(RestaurantDaoLocal.class)
@Remote(RestaurantDao.class)
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class RestaurantBeanDao extends GenericDaoBean<Restaurant, Integer> implements RestaurantDao {

}
