package dao;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import model.User;

@Stateless( name = "UserBean")
@Local(UserDaoLocal.class)
@Remote(UserDao.class)
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class UserBeanDao extends GenericDaoBean<User, Integer> implements UserDaoLocal {
	
	public User findByIdAndRole(int ID, String role) {
		// TODO Auto-generated method stub
		String query = "Select u from User u where u.usrId = "+ID+
						"and u.usrRole = '"+ role + "'";
		List<User> users = findBy(query);
		
		if(users.size() == 1)
			return users.get(0);
		else 
			return null;
	}
	
	public List<User> findAllManagers() {
		// TODO Auto-generated method stub
		String query = "Select u from User u where u.usrRole = 'MN'";
		return findBy(query);
	}
	
	public List<User> findAllUsers() {
		// TODO Auto-generated method stub
		String query = "Select u from User u where u.usrRole = 'US'";
		return findBy(query);
	}
	
	public List<User> findAllAdmins() {
		// TODO Auto-generated method stub
		String query = "Select u from User u where u.usrRole = 'AD'";
		return findBy(query);
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	public void remove(User entity) {
		// TODO Auto-generated method stub
		super.remove(entity);
	}
	
	public List<User> findFriendsOfUser(int usrId) {
		// TODO Auto-generated method stub
		String query = "Select u from User u, Friend f where f.id.usrId = " + usrId
					   +"and u.usrId = f.id.friendUsrId";
		return findBy(query);
	}

	public List<User> findFriendOfUser(int userId, int friendId) {
		
		String query = "Select u from User u, Friend f where f.id.usrId = " + userId
					   +"and f.id.friendUsrId = "+ friendId+ "and u.usrId = f.Id.friendUsrId";
		return findBy(query);
	}

	@Override
	public User findByToken(String token) {
		// TODO Auto-generated method stub
		List<User> usrs = findBy("Select u from User u where u.usrToken = '" + token+"'");
		
		if(usrs.size() == 1)
			return usrs.get(0);
		else
			return null;
	}

	@Override
	public User findByEmail(String email) {
		// TODO Auto-generated method stub
		List<User> usrs = findBy("Select u from User u where u.usrEmail = '" + email +"'");
		
		if(usrs.size() == 1)
			return usrs.get(0);
		else
			return null;
	}

	@Override
	public User prepareUserForSend(User u) {
		// TODO Auto-generated method stub
		User response = new User();
		response.setUsrId(u.getUsrId());
		response.setUsrEmail(u.getUsrEmail());
		response.setUsrName(u.getUsrName());
		response.setUsrSurname(u.getUsrSurname());
		response.setUsrTown(u.getUsrTown());
		response.setUsrAddress(u.getUsrAddress());
		response.setUsrVisits(u.getUsrVisits());
		response.setRestaurant(u.getRestaurant());
		response.setUsrRole(u.getUsrRole());
		
		return response;
	}

	@Override
	public List<User> prepareUsersForSend(List<User> users) {
		// TODO Auto-generated method stub
		ArrayList<User> responseList = new ArrayList<User>();
		
		for(User u : users)
		{
			User response = new User();
			response.setUsrId(u.getUsrId());
			response.setUsrEmail(u.getUsrEmail());
			response.setUsrName(u.getUsrName());
			response.setUsrSurname(u.getUsrSurname());
			response.setUsrTown(u.getUsrTown());
			response.setUsrAddress(u.getUsrAddress());
			response.setUsrVisits(u.getUsrVisits());
			response.setRestaurant(u.getRestaurant());
			response.setUsrRole(u.getUsrRole());
			
			responseList.add(response);
		}
		
		return responseList;
	}

	@Override
	public List<User> findByNameAndSurname(int id, String[] s) {
		// TODO Auto-generated method stub
		ArrayList<User> result = new ArrayList<>();
		String query1 = "Select u from User u where ";
		for (String ss : s)
		{
			query1 += " u.usrName LIKE '%"+ss+"%' or u.usrSurname LIKE '%"+ss+"%' ";
		}
		String query = "Select u from User u where u.usrId <> "+ id + " and u.usrRole = 'US' and u.usrActive = true";
		
		List<User> u1 = findBy(query);
		List<User> u2 = findBy(query1);
		
		for (User u : u1)
		{
			for (User uu: u2)
			{
				if(u.getUsrId() == uu.getUsrId())
					result.add(u);
			}
		}
		
		return result;
	}
}
