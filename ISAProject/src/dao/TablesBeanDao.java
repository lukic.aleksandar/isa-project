package dao;

import java.util.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import model.Reservation;
import model.TablePK;
import model.Tables;
import viewmodel.TabelPK;
import viewmodel.TabelWithStatus;

@Stateless(name = "TablesDao")
@Local(TablesDaoLocal.class)
@Remote(TablesDao.class)
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class TablesBeanDao extends GenericDaoBean<Tables, TablePK> implements TablesDao {
	
	@EJB
	private ReservationDaoLocal reservationDao;
	
	public List<Tables> findAllForRestaurant(int rsId) {
		// TODO Auto-generated method stub
		String query = "Select t from Tables t where t.id.rsId = "+ rsId;
		return findBy(query);
	}

	@Override
	public List<TabelWithStatus> findAllForRestaurantStatus(int rsId, Date start, Date end) {
		// TODO Auto-generated method stub
		List<Tables> tbs = findAllForRestaurant(rsId);
		ArrayList<TabelWithStatus> tbsStatus = new ArrayList<TabelWithStatus>();
		
		if(tbs.size() == 0)
			return tbsStatus;
		
		for(Tables t : tbs)
		{
			TabelWithStatus tws = new TabelWithStatus();
			tws.setId(new TabelPK(t.getId().getRsId(), t.getId().getTbId()));
			tws.setRestaurant(t.getRestaurant());
			tws.setTbCol(t.getTbCol());
			tws.setTbRow(t.getTbRow());
			tws.setTbSeats(t.getTbSeats());
			tws.setTbStatus(false);
			
			String query = "Select r from Reservation r where r.id.rsId = "+tws.getId().getRsId()+
						   " and r.id.tbId = " + tws.getId().getTbId();
			List<Reservation> lr = reservationDao.findBy(query);
			
			if(lr.size() == 0)
			{	
				tws.setTbStatus(false);
				tbsStatus.add(tws);
				continue;
			}	
			boolean check = false;
			for(Reservation r : lr)
			{
				Calendar cal = Calendar.getInstance(); //
			    cal.setTime(r.getRsvDate()); 
			    cal.add(Calendar.HOUR_OF_DAY, r.getRsvDuration()); 
			    Date date = cal.getTime();
			    
			    if(date.getTime() <= end.getTime() && date.getTime() >= start.getTime())
			    {
			    	tws.setTbStatus(true);
			    	tbsStatus.add(tws);
			    	check = true;
			    	break;
			    }
			}
			
			if(!check)
			{
				tws.setTbStatus(false);
		    	tbsStatus.add(tws);
			}
			
		}
		
		return tbsStatus;
	}

}
