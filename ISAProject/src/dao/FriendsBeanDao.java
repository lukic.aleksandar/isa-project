package dao;

import java.util.List;

import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import model.Friend;
import model.FriendPK;
import model.User;

@Stateless( name = "FriendsDao")
@Local(FriendsDaoLocal.class)
@Remote(FriendsDao.class)
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class FriendsBeanDao extends GenericDaoBean<Friend, FriendPK> implements FriendsDao {

}
