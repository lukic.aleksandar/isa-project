package dao;

import java.util.Date;
import java.util.List;

import model.TablePK;
import model.Tables;
import viewmodel.TabelWithStatus;

public interface TablesDao extends GenericDao<Tables, TablePK> {

	public List<Tables> findAllForRestaurant(int rsId);
	public List<TabelWithStatus> findAllForRestaurantStatus(int rsId, Date start, Date end);
}
