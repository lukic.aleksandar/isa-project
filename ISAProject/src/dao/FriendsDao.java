package dao;

import java.util.List;

import model.Friend;
import model.FriendPK;
import model.User;

public interface FriendsDao extends GenericDao<Friend, FriendPK>{
	
}
