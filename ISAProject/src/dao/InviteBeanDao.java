package dao;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import model.Invite;
import model.InvitePK;
import model.User;

@Stateless( name = "InviteDao")
@Local(InviteDaoLocal.class)
@Remote(InviteDao.class)
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class InviteBeanDao extends GenericDaoBean<Invite, InvitePK> implements InviteDao{

	@EJB 
	private UserDaoLocal userDao;
	
	public List<Invite> findInvitationsForUser(int usrId) {
		// TODO Auto-generated method stub
		String query = "Select i from Invite i where i.id.usrId = " + usrId;
		return findBy(query);
	}

	@Override
	public Invite findByToken(String token) {
		String query = "Select i from Invite i where i.inToken = '"+token+"'";
		List<Invite> ins = findBy(query);
		
		if(ins.size() == 1)
			return ins.get(0);
		else
			return null;
	}

	@Override
	public List<User> findFriendsInInvitation(Invite inv) {
		// TODO Auto-generated method stub
		String invQ = "Select a from Invite a where a.id.usrId = "+inv.getId().getUsrId() + " and "
				+" a.id.rsId = " + inv.getId().getRsId() + " and a.id.tbId = " + inv.getId().getTbId()+ " and "+
				"a.id.rsvSerial = "+ inv.getId().getRsvSerial();
		
		List<Invite> invs = findBy(invQ);
		
		ArrayList<User> users = new ArrayList<User>();
		
		for (Invite i : invs)
		{
			users.add(userDao.prepareUserForSend(userDao.findById(i.getId().getFriUsrId())));
		}
		
		return users;
	}

}
