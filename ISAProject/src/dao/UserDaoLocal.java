package dao;

import java.util.List;

import model.User;

public interface UserDaoLocal extends UserDao{
	
	public User findByIdAndRole(int ID, String role);
	public List<User> findAllManagers();
	public List<User> findAllUsers();
	public List<User> findAllAdmins();
	public User prepareUserForSend(User u);
	public List<User> prepareUsersForSend(List<User> users);
}
