package dao;

import javax.ejb.Stateless;

import model.Restaurant;


public interface RestaurantDao extends GenericDao<Restaurant, Integer>{

}
