package dao;

import java.util.List;

import model.Menuitem;
import model.MenuitemPK;

public interface MenuItemDao extends GenericDao<Menuitem, MenuitemPK>{
	
	public List<Menuitem> findAllForRestaurant(int rsID);
	

}
