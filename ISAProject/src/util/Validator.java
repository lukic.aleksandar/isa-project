package util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import model.User;

public class Validator {
	
	public static boolean hasPermission(HttpServletRequest request, String [] permissions)
	{
		HttpSession session = request.getSession();
		User u = (User) session.getAttribute("user");
		System.out.println("USER:::GET");
		System.out.println(session.getAttribute("user"));
		if(u != null)
		{
			for(String p : permissions)
			{
				if(u.getUsrRole().equals(p))
					return true;
			}
		}
				
		return false;
	}
}
