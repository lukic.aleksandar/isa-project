package message;

import java.util.Date;

import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;


@MessageDriven(
		activationConfig = {
			@ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge"),
			@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
			@ActivationConfigProperty(propertyName = "destination", propertyValue = "MailQueue") 
		}
	)
public class MessageBean implements MessageListener {

	@Resource(name="Mail")
	Session session;
	

	public void onMessage(Message message) {
		
	    try {
	        if (message instanceof ObjectMessage) {
	            ObjectMessage obj = (ObjectMessage) message;
	            
	            Object info = obj.getObject();
	            
	            if(info instanceof UserInfo)
	            	sendMessage((UserInfo)info);
	            else if (info instanceof InvitationInfo)
	            	sendInvitation((InvitationInfo)info);
	            // Validate the credit card using web services...
	            
	            
	        } else {
	            System.out.println("MESSAGE BEAN: Message of wrong type: " + message.getClass().getName());
	        }
	    } catch (JMSException e) {
	        e.printStackTrace();
	    } catch (Throwable te) {
	        te.printStackTrace();
	    }
	}

	private void sendMessage(UserInfo info) throws AddressException, MessagingException {

		// Constructs the message 
		javax.mail.Message msg = new MimeMessage(session);
		msg.setFrom(new InternetAddress("pera.p.peric20@gmail.com"));
		msg.setRecipients(RecipientType.TO, InternetAddress.parse(info.getEmail()));
		msg.setSubject("Potvrda registracije");
		msg.setText("Potvrdite registraciju klikom na link : http://192.168.0.10:8080/ISAProject/auth/conf/?token="+info.getToken());
		msg.setSentDate(new Date());
		
		// Sends the message
		Transport.send(msg);

		System.out.println("MESSAGE BEAN: Mail was sent successfully.");
	}
	
	private void sendInvitation(InvitationInfo info) throws AddressException, MessagingException
	{
		javax.mail.Message msg = new MimeMessage(session);
		msg.setFrom(new InternetAddress("pera.p.peric20@gmail.com"));
		msg.setRecipients(RecipientType.TO, InternetAddress.parse(info.getEmail()));
		msg.setSubject("Potvrda dolaska na rezervaciju");
		msg.setText("Potvrdite dolazak klikom na link : http://192.168.0.10:8080/ISAProject/user/invite/conf/?token="+info.getToken());
		msg.setSentDate(new Date());
		
		// Sends the message
		Transport.send(msg);

		System.out.println("MESSAGE BEAN: Mail was sent successfully.");
	}

}
