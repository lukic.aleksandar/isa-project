package model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;


/**
 * The persistent class for the restaurant database table.
 * 
 */
@Entity
@NamedQuery(name="Restaurant.findAll", query="SELECT r FROM Restaurant r")
@JsonIgnoreProperties({"users", "menuitems", "tables"})
public class Restaurant implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="RS_ID")
	private int rsId;

	@Column(name="RS_ADDRESS", length = 150)
	private String rsAddress;

	@Column(name="RS_AVGGRADE")
	private BigDecimal rsAvggrade;

	@Column(name="RS_DESC", length = 500)
	private String rsDesc;

	@Column(name="RS_NAME", nullable = false, length = 100)
	private String rsName;

	@Column(name="RS_TOWN", length = 100)
	private String rsTown;

	//bi-directional many-to-one association to Menuitem
	@OneToMany(mappedBy="restaurant", fetch=FetchType.EAGER)
	private Set<Menuitem> menuitems;

	//bi-directional many-to-one association to Table
	@OneToMany(mappedBy="restaurant", fetch=FetchType.EAGER)
	private Set<Tables> tables;

	//bi-directional many-to-one association to User
	@OneToMany(mappedBy="restaurant", fetch = FetchType.EAGER)
	private Set<User> users;

	public Restaurant() {
	}

	public int getRsId() {
		return this.rsId;
	}

	public void setRsId(int rsId) {
		this.rsId = rsId;
	}

	public String getRsAddress() {
		return this.rsAddress;
	}

	public void setRsAddress(String rsAddress) {
		this.rsAddress = rsAddress;
	}

	public BigDecimal getRsAvggrade() {
		return this.rsAvggrade;
	}

	public void setRsAvggrade(BigDecimal rsAvggrade) {
		this.rsAvggrade = rsAvggrade;
	}

	public String getRsDesc() {
		return this.rsDesc;
	}

	public void setRsDesc(String rsDesc) {
		this.rsDesc = rsDesc;
	}

	public String getRsName() {
		return this.rsName;
	}

	public void setRsName(String rsName) {
		this.rsName = rsName;
	}

	public String getRsTown() {
		return this.rsTown;
	}

	public void setRsTown(String rsTown) {
		this.rsTown = rsTown;
	}

	public Set<Menuitem> getMenuitems() {
		return this.menuitems;
	}

	public void setMenuitems(Set<Menuitem> menuitems) {
		this.menuitems = menuitems;
	}

	public Menuitem addMenuitem(Menuitem menuitem) {
		getMenuitems().add(menuitem);
		menuitem.setRestaurant(this);

		return menuitem;
	}

	public Menuitem removeMenuitem(Menuitem menuitem) {
		getMenuitems().remove(menuitem);
		menuitem.setRestaurant(null);

		return menuitem;
	}

	public Set<Tables> getTables() {
		return this.tables;
	}

	public void setTables(Set<Tables> tables) {
		this.tables = tables;
	}

	public Tables addTable(Tables table) {
		getTables().add(table);
		table.setRestaurant(this);

		return table;
	}

	public Tables removeTable(Tables table) {
		getTables().remove(table);
		table.setRestaurant(null);

		return table;
	}

	public Set<User> getUsers() {
		return this.users;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}

	public User addUser(User user) {
		getUsers().add(user);
		user.setRestaurant(this);

		return user;
	}

	public User removeUser(User user) {
		getUsers().remove(user);
		user.setRestaurant(null);

		return user;
	}
	
	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		if (this == obj)
			return true;
		if(obj instanceof Restaurant)
			return this.rsId == ((Restaurant)obj).rsId;
		else 
			return false;
	}
	
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		final int prime = 31;
		int hash = 17;
		hash = hash*prime + this.rsId;
		
		return hash;
	}
		
}