package model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the tables database table.
 * 
 */
@Embeddable
public class TablePK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;
	
	@Column(name="RS_ID", updatable=false)
	private int rsId;

	@Column(name="TB_ID")
	private int tbId;

	public TablePK() {
	}
	
	
	public TablePK(int rsId, int tbId) {
		super();
		this.rsId = rsId;
		this.tbId = tbId;
	}

	public int getRsId() {
		return this.rsId;
	}
	public void setRsId(int rsId) {
		this.rsId = rsId;
	}
	public int getTbId() {
		return this.tbId;
	}
	public void setTbId(int tbId) {
		this.tbId = tbId;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof TablePK)) {
			return false;
		}
		TablePK castOther = (TablePK)other;
		return 
			(this.rsId == castOther.rsId)
			&& (this.tbId == castOther.tbId);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.rsId;
		hash = hash * prime + this.tbId;
		
		return hash;
	}
}