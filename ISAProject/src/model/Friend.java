package model;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;


/**
 * The persistent class for the friends database table.
 * 
 */
@Entity
@Table(name="friends")
@NamedQuery(name="Friend.findAll", query="SELECT f FROM Friend f")
@JsonIgnoreProperties({"invites"})
public class Friend implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private FriendPK id;

	//bi-directional many-to-one association to Invite
	@OneToMany(mappedBy="friend")
	private Set<Invite> invites;

	public Friend() {
	}

	public FriendPK getId() {
		return this.id;
	}

	public void setId(FriendPK id) {
		this.id = id;
	}

	public Set<Invite> getInvites() {
		return this.invites;
	}

	public void setInvites(Set<Invite> invites) {
		this.invites = invites;
	}

	public Invite addInvite(Invite invite) {
		getInvites().add(invite);
		invite.setFriend(this);

		return invite;
	}

	public Invite removeInvite(Invite invite) {
		getInvites().remove(invite);
		invite.setFriend(null);

		return invite;
	}

}