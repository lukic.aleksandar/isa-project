package model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the invites database table.
 * 
 */
@Embeddable
public class InvitePK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="FRI_USR_ID", updatable=false)
	private int friUsrId;

	@Column(name="RS_ID", updatable=false)
	private int rsId;

	@Column(name="USR_ID", updatable=false)
	private int usrId;

	@Column(name="TB_ID", updatable=false)
	private int tbId;

	@Column(name="RSV_SERIAL", updatable=false)
	private long rsvSerial;

	public InvitePK() {
	}
	
	public InvitePK(int friUsrId, int rsId, int usrId, int tbId, int rsvSerial) {
		super();
		this.friUsrId = friUsrId;
		this.rsId = rsId;
		this.usrId = usrId;
		this.tbId = tbId;
		this.rsvSerial = rsvSerial;
	}

	public int getFriUsrId() {
		return this.friUsrId;
	}
	public void setFriUsrId(int friUsrId) {
		this.friUsrId = friUsrId;
	}
	public int getRsId() {
		return this.rsId;
	}
	public void setRsId(int rsId) {
		this.rsId = rsId;
	}
	public int getUsrId() {
		return this.usrId;
	}
	public void setUsrId(int usrId) {
		this.usrId = usrId;
	}
	public int getTbId() {
		return this.tbId;
	}
	public void setTbId(int tbId) {
		this.tbId = tbId;
	}
	public long getRsvSerial() {
		return this.rsvSerial;
	}
	public void setRsvSerial(int rsvSerial) {
		this.rsvSerial = rsvSerial;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof InvitePK)) {
			return false;
		}
		InvitePK castOther = (InvitePK)other;
		return 
			(this.friUsrId == castOther.friUsrId)
			&& (this.rsId == castOther.rsId)
			&& (this.usrId == castOther.usrId)
			&& (this.tbId == castOther.tbId)
			&& (this.rsvSerial == castOther.rsvSerial);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.friUsrId;
		hash = hash * prime + this.rsId;
		hash = hash * prime + this.usrId;
		hash = hash * prime + this.tbId;
		hash = (int) (hash * prime + this.rsvSerial);
		
		return hash;
	}
}