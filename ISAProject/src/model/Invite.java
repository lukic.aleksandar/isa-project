package model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the invites database table.
 * 
 */
@Entity
@Table(name="invites")
@NamedQuery(name="Invite.findAll", query="SELECT i FROM Invite i")
public class Invite implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private InvitePK id;

	@Column(name="IN_CONF")
	private boolean inConf;

	@Column(name="IN_GRADE")
	private BigDecimal inGrade;
	
	@Column(name = "IN_TOKEN", length = 100)
	private String inToken;

	public String getInToken() {
		return inToken;
	}

	public void setInToken(String inToken) {
		this.inToken = inToken;
	}

	//bi-directional many-to-one association to Friend
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name="FRI_USR_ID", referencedColumnName="USR_ID"),
		@JoinColumn(name="USR_ID", referencedColumnName="USE_USR_ID")
		})
	private Friend friend;

	//bi-directional many-to-one association to Reservation
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name="RS_ID", referencedColumnName="RSV_ID"),
		@JoinColumn(name="RSV_SERIAL", referencedColumnName="RSV_SERIAL"),
		@JoinColumn(name="TB_ID", referencedColumnName="TB_ID"),
		@JoinColumn(name="USR_ID", referencedColumnName="USR_ID")
		})
	private Reservation reservation;

	public Invite() {
	}

	public InvitePK getId() {
		return this.id;
	}

	public void setId(InvitePK id) {
		this.id = id;
	}

	public boolean getInConf() {
		return this.inConf;
	}

	public void setInConf(boolean inConf) {
		this.inConf = inConf;
	}

	public BigDecimal getInGrade() {
		return this.inGrade;
	}

	public void setInGrade(BigDecimal inGrade) {
		this.inGrade = inGrade;
	}

	public Friend getFriend() {
		return this.friend;
	}

	public void setFriend(Friend friend) {
		this.friend = friend;
	}

	public Reservation getReservation() {
		return this.reservation;
	}

	public void setReservation(Reservation reservation) {
		this.reservation = reservation;
	}

}