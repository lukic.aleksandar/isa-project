package model;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;


/**
 * The persistent class for the tables database table.
 * 
 */
@Entity
@Table(name="tables")
@NamedQuery(name="Table.findAll", query="SELECT t FROM Table t")
@JsonIgnoreProperties("reservations")
public class Tables implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private TablePK id;

	@Column(name="TB_COL", nullable = false)
	private int tbCol;

	@Column(name="TB_ROW", nullable = false)
	private int tbRow;

	@Column(name="TB_SEATS")
	private int tbSeats;

	//bi-directional many-to-one association to Reservation
	@OneToMany(mappedBy="table", fetch=FetchType.EAGER)
	private Set<Reservation> reservations;

	//bi-directional many-to-one association to Restaurant
	@ManyToOne
	@JoinColumn(name="RS_ID")
	private Restaurant restaurant;

	public Tables() {
	}

	public TablePK getId() {
		return this.id;
	}

	public void setId(TablePK id) {
		this.id = id;
	}

	public int getTbCol() {
		return this.tbCol;
	}

	public void setTbCol(int tbCol) {
		this.tbCol = tbCol;
	}

	public int getTbRow() {
		return this.tbRow;
	}

	public void setTbRow(int tbRow) {
		this.tbRow = tbRow;
	}

	public int getTbSeats() {
		return this.tbSeats;
	}

	public void setTbSeats(int tbSeats) {
		this.tbSeats = tbSeats;
	}

	public Set<Reservation> getReservations() {
		return this.reservations;
	}

	public void setReservations(Set<Reservation> reservations) {
		this.reservations = reservations;
	}

	public Reservation addReservation(Reservation reservation) {
		getReservations().add(reservation);
		reservation.setTable(this);

		return reservation;
	}

	public Reservation removeReservation(Reservation reservation) {
		getReservations().remove(reservation);
		reservation.setTable(null);

		return reservation;
	}

	public Restaurant getRestaurant() {
		return this.restaurant;
	}

	public void setRestaurant(Restaurant restaurant) {
		this.restaurant = restaurant;
	}

}