package model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the menuitems database table.
 * 
 */
@Embeddable
public class MenuitemPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="RS_ID", updatable=false)
	private int rsId;

	@Column(name="MI_ID")
	private int miId;

	public MenuitemPK() {
	}
	
	public MenuitemPK(int rsId, int miId) {
		super();
		this.rsId = rsId;
		this.miId = miId;
	}

	public int getRsId() {
		return this.rsId;
	}
	public void setRsId(int rsId) {
		this.rsId = rsId;
	}
	public int getMiId() {
		return this.miId;
	}
	public void setMiId(int miId) {
		this.miId = miId;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof MenuitemPK)) {
			return false;
		}
		MenuitemPK castOther = (MenuitemPK)other;
		return 
			(this.rsId == castOther.rsId)
			&& (this.miId == castOther.miId);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.rsId;
		hash = hash * prime + this.miId;
		
		return hash;
	}
}