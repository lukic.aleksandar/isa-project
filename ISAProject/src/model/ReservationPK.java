package model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the reservation database table.
 * 
 */
@Embeddable
public class ReservationPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="RSV_ID", updatable=false)
	private int rsId;

	@Column(name="USR_ID", updatable=false)
	private int usrId;

	@Column(name="TB_ID", updatable=false)
	private int tbId;
	
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="RSV_SERIAL")
	private long rsvSerial;

	public ReservationPK() {
	}
	
	public ReservationPK(int rsId, int usrId, int tbId, long rsvSerial) {
		super();
		this.rsId = rsId;
		this.usrId = usrId;
		this.tbId = tbId;
		this.rsvSerial = rsvSerial;
	}

	public int getRsId() {
		return this.rsId;
	}
	public void setRsId(int rsId) {
		this.rsId = rsId;
	}
	public int getUsrId() {
		return this.usrId;
	}
	public void setUsrId(int usrId) {
		this.usrId = usrId;
	}
	public int getTbId() {
		return this.tbId;
	}
	public void setTbId(int tbId) {
		this.tbId = tbId;
	}
	public long getRsvSerial() {
		return this.rsvSerial;
	}
	public void setRsvSerial(long rsvSerial) {
		this.rsvSerial = rsvSerial;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof ReservationPK)) {
			return false;
		}
		ReservationPK castOther = (ReservationPK)other;
		return 
			(this.rsId == castOther.rsId)
			&& (this.usrId == castOther.usrId)
			&& (this.tbId == castOther.tbId)
			&& (this.rsvSerial == castOther.rsvSerial);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.rsId;
		hash = hash * prime + this.usrId;
		hash = hash * prime + this.tbId;
		hash = (int) (hash * prime + this.rsvSerial);
		
		return hash;
	}
}