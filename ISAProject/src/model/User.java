package model;

import java.io.Serializable;
import java.util.Set;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the user database table.
 * 
 */
@Entity
@Table(name = "user")
@JsonIgnoreProperties({"users1","users2", "reservations"})
@NamedQuery(name="User.findAll", query="SELECT u FROM User u")
public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="USR_ID")
	private int usrId;

	@Column(name="USR_ADDRESS", length = 100)
	private String usrAddress;

	@Column(name="USR_EMAIL", unique = true, nullable = false, length = 350)
	private String usrEmail;

	@Column(name="USR_NAME", length = 100)
	private String usrName;

	@Column(name="USR_ROLE", nullable = false, length = 2)
	private String usrRole;

	@Column(name="USR_SURNAME", length = 100)
	private String usrSurname;

	@Column(name="USR_TOWN", length = 100)
	private String usrTown;

	@Column(name="USR_VISITS")
	private int usrVisits;
	
	@Column(name = "USR_PASS", nullable = false, length = 100)
	private String usrPass;
	
	@Column(name = "USR_TOKEN", length = 100)
	private String usrToken;
	
	@Column(name = "USR_ACTIVE")
	private boolean usrActive;
	
	public String getUsrToken() {
		return usrToken;
	}

	public void setUsrToken(String usrToken) {
		this.usrToken = usrToken;
	}

	public boolean isUsrActive() {
		return usrActive;
	}

	public void setUsrActive(boolean usrActive) {
		this.usrActive = usrActive;
	}

	//bi-directional many-to-many association to User
	@ManyToMany(fetch=FetchType.EAGER)
	@JoinTable(
		name="friends",
		 joinColumns={
			@JoinColumn(name="USR_ID")
			}
		, inverseJoinColumns={
			@JoinColumn(name="USE_USR_ID")
			}
		)
	private Set<User> users1;

	//bi-directional many-to-many association to User
	@ManyToMany(mappedBy="users1", fetch=FetchType.EAGER)
	private Set<User> users2;

	//bi-directional many-to-one association to Reservation
	@OneToMany(mappedBy="user")
	private Set<Reservation> reservations;

	//bi-directional many-to-one association to Restaurant
	@ManyToOne(cascade = CascadeType.MERGE)
	@JoinColumn(name="RS_ID")
	private Restaurant restaurant;
	
	public User() {
	}

	public int getUsrId() {
		return this.usrId;
	}

	public void setUsrId(int usrId) {
		this.usrId = usrId;
	}

	public String getUsrAddress() {
		return this.usrAddress;
	}

	public void setUsrAddress(String usrAddress) {
		this.usrAddress = usrAddress;
	}

	public String getUsrEmail() {
		return this.usrEmail;
	}

	public void setUsrEmail(String usrEmail) {
		this.usrEmail = usrEmail;
	}

	public String getUsrName() {
		return this.usrName;
	}

	public void setUsrName(String usrName) {
		this.usrName = usrName;
	}

	public String getUsrRole() {
		return this.usrRole;
	}

	public void setUsrRole(String usrRole) {
		this.usrRole = usrRole;
	}

	public String getUsrSurname() {
		return this.usrSurname;
	}

	public void setUsrSurname(String usrSurname) {
		this.usrSurname = usrSurname;
	}

	public String getUsrTown() {
		return this.usrTown;
	}

	public void setUsrTown(String usrTown) {
		this.usrTown = usrTown;
	}

	public int getUsrVisits() {
		return this.usrVisits;
	}

	public void setUsrVisits(int usrVisits) {
		this.usrVisits = usrVisits;
	}
	

	public String getUsrPass() {
		return usrPass;
	}

	public void setUsrPass(String usrPass) {
		this.usrPass = usrPass;
	}
	
	public Set<User> getUsers1() {
		return this.users1;
	}

	public void setUsers1(Set<User> users1) {
		this.users1 = users1;
	}

	public Set<User> getUsers2() {
		return this.users2;
	}

	public void setUsers2(Set<User> users2) {
		this.users2 = users2;
	}

	public Set<Reservation> getReservations() {
		return this.reservations;
	}

	public void setReservations(Set<Reservation> reservations) {
		this.reservations = reservations;
	}

	public Reservation addReservation(Reservation reservation) {
		getReservations().add(reservation);
		reservation.setUser(this);

		return reservation;
	}

	public Reservation removeReservation(Reservation reservation) {
		getReservations().remove(reservation);
		reservation.setUser(null);

		return reservation;
	}

	public Restaurant getRestaurant() {
		return this.restaurant;
	}

	public void setRestaurant(Restaurant restaurant) {
		this.restaurant = restaurant;
	}
	
	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		
		if(this == obj)
			return true;
		
		if(obj instanceof User)
			return this.usrId == ((User)obj).usrId;
		else
			return false;
	}
	
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		final int prime = 31;
		int hash = 17;
		hash = hash*prime + this.usrId;
		
		return hash;
		
	}

}