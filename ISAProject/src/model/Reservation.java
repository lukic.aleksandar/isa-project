package model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Time;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;


/**
 * The persistent class for the reservation database table.
 * 
 */
@Entity
@Table (name = "reservation")
@NamedQuery(name="Reservation.findAll", query="SELECT r FROM Reservation r")
@JsonIgnoreProperties({"invites"})
public class Reservation implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private ReservationPK id;

	@Column(name="RSV_AVGGRADE")
	private BigDecimal rsvAvggrade;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="RSV_DATE")
	private Date rsvDate;

	@Column(name="RSV_DURATION")
	private int rsvDuration;

	@Column(name="RSV_GRADE")
	private BigDecimal rsvGrade;

	@Column(name="RSV_STATUS")
	private byte rsvStatus;

	//bi-directional many-to-one association to Invite
	@OneToMany(mappedBy="reservation")
	private Set<Invite> invites;

	//bi-directional many-to-one association to Table
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name="RSV_ID", referencedColumnName="RS_ID"),
		@JoinColumn(name="TB_ID", referencedColumnName="TB_ID")
		})
	private Tables table;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name="USR_ID")
	private User user;

	public Reservation() {
	}

	public ReservationPK getId() {
		return this.id;
	}

	public void setId(ReservationPK id) {
		this.id = id;
	}

	public BigDecimal getRsvAvggrade() {
		return this.rsvAvggrade;
	}

	public void setRsvAvggrade(BigDecimal rsvAvggrade) {
		this.rsvAvggrade = rsvAvggrade;
	}

	public Date getRsvDate() {
		return this.rsvDate;
	}

	public void setRsvDate(Date rsvDate) {
		this.rsvDate = rsvDate;
	}

	public int getRsvDuration() {
		return this.rsvDuration;
	}

	public void setRsvDuration(int rsvDuration) {
		this.rsvDuration = rsvDuration;
	}

	public BigDecimal getRsvGrade() {
		return this.rsvGrade;
	}

	public void setRsvGrade(BigDecimal rsvGrade) {
		this.rsvGrade = rsvGrade;
	}

	public byte getRsvStatus() {
		return this.rsvStatus;
	}

	public void setRsvStatus(byte rsvStatus) {
		this.rsvStatus = rsvStatus;
	}

	public Set<Invite> getInvites() {
		return this.invites;
	}

	public void setInvites(Set<Invite> invites) {
		this.invites = invites;
	}

	public Invite addInvite(Invite invite) {
		getInvites().add(invite);
		invite.setReservation(this);

		return invite;
	}

	public Invite removeInvite(Invite invite) {
		getInvites().remove(invite);
		invite.setReservation(null);

		return invite;
	}

	public Tables getTable() {
		return this.table;
	}

	public void setTable(Tables table) {
		this.table = table;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}