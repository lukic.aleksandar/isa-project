package model;

import java.io.Serializable;
import javax.persistence.Table;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the menuitems database table.
 * 
 */
@Entity
@Table(name="menuitems")
@NamedQuery(name="Menuitem.findAll", query="SELECT m FROM Menuitem m")
public class Menuitem implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private MenuitemPK id;

	@Column(name="MI_DESC", length = 500)
	private String miDesc;

	@Column(name="MI_NAME", length = 100)
	private String miName;

	@Column(name="MI_PRICE")
	private BigDecimal miPrice;

	//bi-directional many-to-one association to Restaurant
	@ManyToOne
	@JoinColumn(name="RS_ID")
	private Restaurant restaurant;

	public Menuitem() {
	}

	public MenuitemPK getId() {
		return this.id;
	}

	public void setId(MenuitemPK id) {
		this.id = id;
	}

	public String getMiDesc() {
		return this.miDesc;
	}

	public void setMiDesc(String miDesc) {
		this.miDesc = miDesc;
	}

	public String getMiName() {
		return this.miName;
	}

	public void setMiName(String miName) {
		this.miName = miName;
	}

	public BigDecimal getMiPrice() {
		return this.miPrice;
	}

	public void setMiPrice(BigDecimal miPrice) {
		this.miPrice = miPrice;
	}

	public Restaurant getRestaurant() {
		return this.restaurant;
	}

	public void setRestaurant(Restaurant restaurant) {
		this.restaurant = restaurant;
	}

}