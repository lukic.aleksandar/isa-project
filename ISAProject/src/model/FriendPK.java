package model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the friends database table.
 * 
 */
@Embeddable
public class FriendPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="USE_USR_ID", updatable=false)
	private int usrId;

	@Column(name="USR_ID", updatable=false)
	private int friendUsrId;

	public FriendPK() {
	}
	
	public FriendPK(int usrId, int friendUsrId) {
		super();
		this.usrId = usrId;
		this.friendUsrId = friendUsrId;
	}

	public int getUsrId() {
		return this.usrId;
	}
	public void setUsrId(int useUsrId) {
		this.usrId = useUsrId;
	}
	public int getFriendUsrId() {
		return this.friendUsrId;
	}
	public void setFriendUsrId(int usrId) {
		this.friendUsrId = usrId;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof FriendPK)) {
			return false;
		}
		FriendPK castOther = (FriendPK)other;
		return 
			(this.usrId == castOther.usrId)
			&& (this.friendUsrId == castOther.friendUsrId);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.usrId;
		hash = hash * prime + this.friendUsrId;
		
		return hash;
	}
}