package viewmodel;

import java.util.ArrayList;
import java.util.Date;

import model.Restaurant;
import model.User;

public class InviteDetails {
	
	public ArrayList<User> friends;
	public Restaurant restaurant;
	public User user;
	public Date date;
	public int time;
	public int table;
	
	public int getTable() {
		return table;
	}
	public void setTable(int table) {
		this.table = table;
	}
	public InviteDetails() {
		super();
		// TODO Auto-generated constructor stub
	}
	public ArrayList<User> getFriends() {
		return friends;
	}
	public void setFriends(ArrayList<User> friends) {
		this.friends = friends;
	}
	public Restaurant getRestaurant() {
		return restaurant;
	}
	public void setRestaurant(Restaurant restaurant) {
		this.restaurant = restaurant;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public int getTime() {
		return time;
	}
	public void setTime(int time) {
		this.time = time;
	}
	
	
	
}
