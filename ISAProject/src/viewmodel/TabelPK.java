package viewmodel;

import model.TablePK;

public class TabelPK {
	
	
	private int rsId;

	
	private int tbId;

	public TabelPK() {
	}
	
	
	public TabelPK(int rsId, int tbId) {
		super();
		this.rsId = rsId;
		this.tbId = tbId;
	}

	public int getRsId() {
		return this.rsId;
	}
	public void setRsId(int rsId) {
		this.rsId = rsId;
	}
	public int getTbId() {
		return this.tbId;
	}
	public void setTbId(int tbId) {
		this.tbId = tbId;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof TabelPK)) {
			return false;
		}
		TabelPK castOther = (TabelPK)other;
		return 
			(this.rsId == castOther.rsId)
			&& (this.tbId == castOther.tbId);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.rsId;
		hash = hash * prime + this.tbId;
		
		return hash;
	}
}
