package viewmodel;

import model.Restaurant;
import model.TablePK;

public class TabelWithStatus {

	
	private TabelPK id;
	private int tbCol;
	private boolean tbStatus;
	private int tbRow;
	private int tbSeats;
	private Restaurant restaurant;

	public TabelWithStatus() {
	}

	public boolean isTbStatus() {
		return tbStatus;
	}

	public void setTbStatus(boolean tbStatus) {
		this.tbStatus = tbStatus;
	}
	public void setId(TabelPK id) {
		this.id = id;
	}

	public TabelPK getId() {
		return id;
	}

	public int getTbCol() {
		return this.tbCol;
	}

	public void setTbCol(int tbCol) {
		this.tbCol = tbCol;
	}

	public int getTbRow() {
		return this.tbRow;
	}

	public void setTbRow(int tbRow) {
		this.tbRow = tbRow;
	}

	public int getTbSeats() {
		return this.tbSeats;
	}

	public void setTbSeats(int tbSeats) {
		this.tbSeats = tbSeats;
	}

	public Restaurant getRestaurant() {
		return this.restaurant;
	}

	public void setRestaurant(Restaurant restaurant) {
		this.restaurant = restaurant;
	}
}
