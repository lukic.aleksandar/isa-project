package controller;

import java.io.IOException;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import dao.RestaurantDaoLocal;
import dao.TablesDaoLocal;
import dao.UserDaoLocal;
import model.Restaurant;
import model.Tables;
import model.User;
import util.Validator;


@Path("/admin")
public class AdminController {
	
	@EJB
	private UserDaoLocal userDao;
	
	@EJB
	private RestaurantDaoLocal restaurantDao;
	
	@Path("/managers")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String addManager(@Context HttpServletRequest request, String json)
	{   
		if(!Validator.hasPermission(request, new String[]{"AD"}))
			return "{\"status\": 401}";
		
		ObjectMapper om = new ObjectMapper();
	
		try {
			
			User u = (User) om.readValue(json, User.class);
			u.setUsrRole("MN");
			u.setUsrActive(true);
			return om.writeValueAsString(userDao.prepareUserForSend(userDao.persist(u)));
			
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "{\"status\": 500}";
	}
	
	@Path("/managers")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getManagers(@Context HttpServletRequest request)
	{
		if(!Validator.hasPermission(request, new String[]{"AD"}))
			return "{\"status\": 401}";
		
		ObjectMapper om = new ObjectMapper();
		
		try {
		
			return om.writeValueAsString(userDao.prepareUsersForSend(userDao.findAllManagers()));
		
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "{\"status\": 500}";
	}
	
	@Path("/managers/{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getManagerById(@Context HttpServletRequest request, @PathParam("id") int managerId)
	{

		if(!Validator.hasPermission(request, new String[]{"AD"}))
			return "{\"status\": 401}";
		
		ObjectMapper om = new ObjectMapper();
		
		try {
		
			return om.writeValueAsString(userDao.prepareUserForSend(userDao.findByIdAndRole(managerId,"MN")));
		
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "{\"status\": 500}";
	}
	
	@Path("/managers/{id}")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String updateManager(@Context HttpServletRequest request, @PathParam("id") int managerId, String json)
	{
		if(!Validator.hasPermission(request, new String[]{"AD"}))
			return "{\"status\": 401}";
		
		ObjectMapper om = new ObjectMapper();
		
		try {
			
			User user = om.readValue(json, User.class);
			user.setUsrId(managerId);
			User u = userDao.findById(user.getUsrId());
			
			if(u != null)
			{	
				user.setUsrRole(u.getUsrRole());
				user.setUsrActive(u.isUsrActive());
				user = userDao.merge(user);
			}
			return om.writeValueAsString(userDao.prepareUserForSend(user));
		
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "{\"status\": 500}";
	}
	
	@Path("/managers/{id}")
	@DELETE
	public void deleteManager(@Context HttpServletRequest request , @PathParam("id") int managerId)
	{
		if(!Validator.hasPermission(request, new String[]{"AD"}))
			return;
		
		try {
			User u = userDao.findByIdAndRole(managerId, "MN");	
			userDao.findByIdAndRemove(u.getUsrId());
		
		} catch(Exception e){
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Path("/restaurants")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String addRestaurant(@Context HttpServletRequest request, String json)
	{
		
		if(!Validator.hasPermission(request, new String[]{"AD"}))
			return "{\"status\": 401}";
		
		ObjectMapper om = new ObjectMapper();
		
		try {
			
			Restaurant rest = (Restaurant) om.readValue(json, Restaurant.class);
			return om.writeValueAsString(restaurantDao.persist(rest));
			
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "{\"status\": 500}";
	}
	
	@Path("/restaurants")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getRestaurants(@Context HttpServletRequest request)
	{
		if(!Validator.hasPermission(request, new String[]{"AD", "US"}))
			return "{\"status\": 401}";
		
		ObjectMapper om = new ObjectMapper();
		
		try {
		
			return om.writeValueAsString(restaurantDao.findAll());
		
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "{\"status\": 500}";
	}
	
	@Path("/restaurants/{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getRestaurantById(@Context HttpServletRequest request, @PathParam("id") int restId)
	{

		if(!Validator.hasPermission(request, new String[]{"AD","US","MN"}))
			return "{\"status\": 401}";
		
		ObjectMapper om = new ObjectMapper();
		
		try {
			Restaurant rst = restaurantDao.findById(restId);
			System.out.println(rst.getUsers().size());
			return om.writeValueAsString(rst);
		
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "{\"status\": 500}";
	}
	
	@Path("/restaurants/{id}")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String updateRestaurant(@Context HttpServletRequest request, @PathParam("id") int restId, String json)
	{

		if(!Validator.hasPermission(request, new String[]{"AD", "MN"}))
			return "{\"status\": 401}";
		
		ObjectMapper om = new ObjectMapper();
		
		try {
		
			Restaurant rest = om.readValue(json, Restaurant.class);
			rest.setRsId(restId);
			
			return om.writeValueAsString(restaurantDao.merge(rest));
		
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "{\"status\": 500}";
	}
	
	@Path("/restaurants/{id}")
	@DELETE
	public void deleteRestairant(@Context HttpServletRequest request, @PathParam("id") int restId)
	{

		if(!Validator.hasPermission(request, new String[]{"AD"}))
			return;
		
		try {
		
			restaurantDao.findByIdAndRemove(restId);
		} catch(Exception e){
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	@Path("/users")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getUsers(@Context HttpServletRequest request)
	{
		if(!Validator.hasPermission(request, new String[]{"AD", "US"}))
			return "{\"status\": 401}";
		
		ObjectMapper om = new ObjectMapper();
		
		try {
		
			return om.writeValueAsString(userDao.prepareUsersForSend(userDao.findAllUsers()));
		
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "{\"status\": 500}";
	}
	
	@Path("/users/{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getUserById(@Context HttpServletRequest request,@PathParam("id") int usrId)
	{
		if(!Validator.hasPermission(request, new String[]{"AD", "US"}))
			return "{\"status\": 401}";
		
		ObjectMapper om = new ObjectMapper();
		
		try {
		
			return om.writeValueAsString(userDao.prepareUserForSend(userDao.findByIdAndRole(usrId,"US")));
		
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "{\"status\": 500}";
	}
	
	@Path("/users/{id}")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String updateUser(@Context HttpServletRequest request, @PathParam("id") int usrId, String json)
	{
		if(!Validator.hasPermission(request, new String[]{"AD", "US"}))
			return "{\"status\": 401}";
		
		ObjectMapper om = new ObjectMapper();
		
		try {
			
			User user = om.readValue(json, User.class);
			user.setUsrId(usrId);
	
			User u = userDao.findById(user.getUsrId());
			
			if(u != null)
			{	
				user.setUsrRole(u.getUsrRole());
				user.setUsrActive(u.isUsrActive());
				user = userDao.merge(user);
			}
			return om.writeValueAsString(userDao.prepareUserForSend(user));
		
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "{\"status\": 500}";
	}
	
	@Path("/users/search")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String searchUsers(@Context HttpServletRequest request, @QueryParam("search") String search)
	{
		if(!Validator.hasPermission(request, new String[]{"AD", "US"}))
			return "{\"status\": 401}";
		
		ObjectMapper om = new ObjectMapper();

		try {
			
			String [] s; 
			if(search.contains(" "))	
			   s = search.split(" ");
			else
			   s = new String []{search};
			
			User u = (User) request.getSession().getAttribute("user");
			return om.writeValueAsString(userDao.prepareUsersForSend(userDao.findByNameAndSurname(u.getUsrId(),s)));
		
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "{\"status\": 500}";
	}	
	
}
