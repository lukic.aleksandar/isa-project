package controller;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.lang.RandomStringUtils;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import dao.UserDaoLocal;
import message.UserInfo;
import model.User;

@Path("/auth")
public class AuthController {
	
	@Resource(name="JmsConnectionFactory")
	private ConnectionFactory qcf;

	@Resource(name="MailQueue")
	private Queue mailQueue;
	
	@EJB
	private UserDaoLocal userDao;
	
	@Path("/register")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public String registerUser(String json)
	{
		ObjectMapper om = new ObjectMapper();
		
		try {
			User u = om.readValue(json, User.class);
			u.setUsrRole("US");
			u = userDao.persist(u);

			if(u != null)
			{
				u.setUsrToken(u.getUsrId()+RandomStringUtils.random(49, true, true));
				u = userDao.merge(u);
				
				UserInfo info = new UserInfo(u.getUsrEmail(), u.getUsrToken());
				
				Connection connection = null;
				Session session = null;
				MessageProducer producer = null;
			
				// Creates a connection
				connection = qcf.createConnection();

	            // Creates a session
	            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

	            // Creates a message producer from the Session to the Topic or Queue
	            producer = session.createProducer(mailQueue);

	            // Creates an object message
	            ObjectMessage object = session.createObjectMessage();
	            object.setObject(info);
			    
	            // Tells the producer to send the object message
	            producer.send(object);
	            
	            // Closes the producer
	            producer.close();
	            
	            // Closes the session
	            session.close();
	            
	            // Closes the connection
	            connection.close();
	            
	            return "{\"status\":200}";
			}
			
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "{\"status\": 500}";
	}
	
	@Path("/conf")
	@GET
	public Response confirmRegistration(@QueryParam("token") String token)
	{
		if(token != null)
		{
			User u = userDao.findByToken(token);
			
			if(u != null)
			{	
				u.setUsrActive(true);
				userDao.merge(u);
				
				URI location = null;
				try {
					location = new URI("http://localhost:8080/ISAProject/#/login"); 
				} catch (URISyntaxException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				if(location == null)
					return null;
				return Response.temporaryRedirect(location).build();
			}
		}
		return null;
	}
	
	@Path("/login")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String loginUser(@Context HttpServletRequest request, String json)
	{
		ObjectMapper om = new ObjectMapper();
		HttpSession session = request.getSession();
		
		if(session.getAttribute("user")!= null)
			return "{\"status\": 200}";
		
		try {
			User usr = om.readValue(json, User.class);
			
			User u = userDao.findByEmail(usr.getUsrEmail());
			
			if(u != null && usr.getUsrPass().equals(u.getUsrPass()) && u.isUsrActive())
			{	
				session.setAttribute("user", u);
				User response = userDao.prepareUserForSend(u);
				return om.writeValueAsString(response);
			}
			return "{\"status\": 401}";
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "{\"status\": 500}";
	}
	
	@Path("/logout")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String logioutUser(@Context HttpServletRequest request, String json)
	{
		request.getSession().invalidate();
		return "{\"status\": 200}";	
	}
	
	@Path("/user")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getLoggedUser(@Context HttpServletRequest request)
	{
		ObjectMapper om = new ObjectMapper();
		
		User u = (User) request.getSession().getAttribute("user");
		
		try {
			
			if(u == null)
				return "{\"status\": 404}";
			User response = userDao.findById(u.getUsrId());
			
			if(response == null)
			{	
				request.getSession().removeAttribute("user");
				return "{\"status\": 404}";
			}	
			
			response = userDao.prepareUserForSend(response);
			return om.writeValueAsString(response);
		
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "{\"status\": 500}";	
	}
	
	
}
