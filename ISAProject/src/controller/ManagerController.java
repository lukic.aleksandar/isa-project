package controller;

import java.io.IOException;
import java.util.Date;
import java.util.Calendar;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import dao.MenuItemDaoLocal;
import dao.TablesDaoLocal;
import model.Menuitem;
import model.MenuitemPK;
import model.TablePK;
import model.Tables;
import util.Validator;
import viewmodel.TabelWithStatus;

@Path("/manager")
public class ManagerController {
	
	@EJB
	private MenuItemDaoLocal menuItemDao;
	
	@EJB 
	private TablesDaoLocal tablesDao;
	
	
	@Path("{rsId}/tables/")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String addTables(@Context HttpServletRequest request, @PathParam("rsId") int rsId, String json)
	{

		if(!Validator.hasPermission(request, new String[]{"AD", "MN"}))
			return "{\"status\": 401}";
		
		ObjectMapper om = new ObjectMapper();
		
		try {
		    Tables tb = om.readValue(json, Tables.class);
			tb.getId().setRsId(rsId);
			return om.writeValueAsString(tablesDao.persist(tb));
			
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return "{\"status\": 500}";
	}
	
	@Path("/{rsId}/tables")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getTables(@Context HttpServletRequest request, @PathParam("rsId") int rsId)
	{
		
		if(!Validator.hasPermission(request, new String[]{"AD", "MN", "US"}))
			return "{\"status\": 401}";
		
		ObjectMapper om = new ObjectMapper();
		
		try {
		
			return om.writeValueAsString(tablesDao.findAllForRestaurant(rsId));
		
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "{\"status\": 500}";
	}
	
	@Path("{rsId}/tables/{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String addTableById(@Context HttpServletRequest request, @PathParam("rsId") int rsId, @PathParam("id") int tbId)
	{

		if(!Validator.hasPermission(request, new String[]{"AD", "MN"}))
			return "{\"status\": 401}";
		
		ObjectMapper om = new ObjectMapper();
		
		try {
			TablePK tpk = new TablePK(rsId, tbId);
			return om.writeValueAsString(tablesDao.findById(tpk));
		
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "{\"status\": 500}";
	}
	
	@Path("{rsId}/tables/{id}")
	@DELETE
	public void deleteTable(@Context HttpServletRequest request, @PathParam("rsId") int rsId, @PathParam("id") int tbId)
	{

		if(!Validator.hasPermission(request, new String[]{"AD", "MN"}))
			return;
		
		try {
			
			tablesDao.findByIdAndRemove(new TablePK(rsId, tbId));
		
		} catch(Exception e){
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	@Path("{rsId}/menuitems/")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String addMenuitem(@Context HttpServletRequest request, @PathParam("rsId") int rsId, String json)
	{

		if(!Validator.hasPermission(request, new String[]{"AD", "MN"}))
			return "{\"status\": 401}";
		
		ObjectMapper om = new ObjectMapper();
		
		try {
			
			Menuitem mi = (Menuitem) om.readValue(json, Menuitem.class);
			mi.getId().setRsId(rsId);
			System.out.println(json);
			System.out.println(mi.getMiName()+":" +mi.getId().getMiId()+":"+mi.getId().getRsId());
			return om.writeValueAsString(menuItemDao.persist(mi));
			
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return "{\"status\": 500}";
	}
	
	@Path("/{rsId}/menuitems")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getMenuitems(@Context HttpServletRequest request, @PathParam("rsId") int rsId)
	{
		
		if(!Validator.hasPermission(request, new String[]{"AD", "MN"}))
			return "{\"status\": 401}";
		
		ObjectMapper om = new ObjectMapper();
		
		try {
		
			return om.writeValueAsString(menuItemDao.findAllForRestaurant(rsId));
		
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "{\"status\": 500}";
	}
	
	@Path("{rsId}/menuitems/{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String addMenuitemById(@Context HttpServletRequest request, @PathParam("rsId") int rsId, @PathParam("id") int miId)
	{
		if(!Validator.hasPermission(request, new String[]{"AD", "MN"}))
			return "{\"status\": 401}";
		
		ObjectMapper om = new ObjectMapper();
		
		try {
			MenuitemPK mipk = new MenuitemPK(rsId, miId);
			return om.writeValueAsString(menuItemDao.findById(mipk));
		
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "{\"status\": 500}";
	}
	
	@Path("{rsId}/menuitems/{id}")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String updateMenuitem(@Context HttpServletRequest request, @PathParam("rsId") int rsId,@PathParam("id") int miId, String json)
	{

		if(!Validator.hasPermission(request, new String[]{"AD", "MN"}))
			return "{\"status\": 401}";
		
		ObjectMapper om = new ObjectMapper();
		
		try {
			
			Menuitem mi = om.readValue(json, Menuitem.class);
			mi.setId(new MenuitemPK(rsId,miId));
			mi = menuItemDao.merge(mi);
			
			return om.writeValueAsString(mi);
		
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "{\"status\": 500}";
	}
	
	@Path("{rsId}/menuitems/{id}")
	@DELETE
	public void deleteMenuitem(@Context HttpServletRequest request, @PathParam("rsId") int rsId, @PathParam("id") int miId)
	{

		if(!Validator.hasPermission(request, new String[]{"AD", "MN"}))
			return;
		try {
			
			menuItemDao.findByIdAndRemove(new MenuitemPK(rsId, miId));
		
		} catch(Exception e){
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Path("{rsId}/tables/status")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getRestaurantsWithStatus(@Context HttpServletRequest request,@PathParam("rsId") int rsId,
			@QueryParam("dt") long dt,@QueryParam("t") int time)
	{
		if(!Validator.hasPermission(request, new String[]{"AD", "US"}))
			return "{\"status\": 401}";
		
		ObjectMapper om = new ObjectMapper();
		try {
			
			Calendar cal = Calendar.getInstance(); 
		    cal.setTime(new Date(dt));
		    Date start = cal.getTime();
		   
		    cal.add(Calendar.HOUR_OF_DAY, time); 
		    Date end = cal.getTime();
			List<TabelWithStatus> rests = tablesDao.findAllForRestaurantStatus(rsId, start, end); 
			
			return om.writeValueAsString(rests);
		
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "{\"status\": 500}";
	}
}
