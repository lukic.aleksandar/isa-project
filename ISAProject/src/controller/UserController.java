package controller;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.openjpa.persistence.jest.JSONObject;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import dao.FriendsDaoLocal;
import dao.InviteDaoLocal;
import dao.ReservationDaoLocal;
import dao.RestaurantDaoLocal;
import dao.UserDaoLocal;
import message.InvitationInfo;
import model.Friend;
import model.FriendPK;
import model.Invite;
import model.InvitePK;
import model.Reservation;
import model.ReservationPK;
import model.User;
import util.Validator;
import viewmodel.InviteDetails;

@Path("/user")
public class UserController {

	@EJB
	private FriendsDaoLocal friendsDao;
	
	@EJB 
	private UserDaoLocal userDao;
	
	@EJB
	private ReservationDaoLocal reservationDao;
	
	@EJB
	private InviteDaoLocal inviteDao;
	
	@EJB
	private RestaurantDaoLocal restaurantDao;

	@Resource(name="JmsConnectionFactory")
	private ConnectionFactory qcf;

	@Resource(name="MailQueue")
	private Queue mailQueue;
	
	
	@Path("/{id}/friends")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String addFriend(@Context HttpServletRequest request, @PathParam("id") int usrId, String json)
	{
		if(!Validator.hasPermission(request, new String[]{"AD", "US"}))
			return "{\"status\": 401}";
		
		ObjectMapper om = new ObjectMapper();
		
		try {
		    User friend = om.readValue(json, User.class);
		    friend = userDao.findByIdAndRole(friend.getUsrId(),"US");
		    
			User user = userDao.findByIdAndRole(usrId, "US");
			
			if(user != null && friend != null &&
			   user.getUsrId() != friend.getUsrId())
			{
				Friend f = new Friend();
				f.setId(new FriendPK(user.getUsrId(), friend.getUsrId()));
				
				Friend f1 = new Friend();
				f1.setId(new FriendPK(friend.getUsrId(), user.getUsrId()));
			
				Friend persisted = friendsDao.persist(f);
				friendsDao.persist(f1);
	
				return om.writeValueAsString(persisted);
			}
		    
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return "{\"status\": 500}";	
	}
	
	@Path("/{id}/friends")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getFriends(@Context HttpServletRequest request, @PathParam("id") int usrId)
	{
		if(!Validator.hasPermission(request, new String[]{"AD", "US"}))
			return "{\"status\": 401}";
		
		ObjectMapper om = new ObjectMapper();
		
		try {
			
			return om.writeValueAsString(userDao.prepareUsersForSend(userDao.findFriendsOfUser(usrId)));
			
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return "{\"status\": 500}";	
	
	}
	
	@Path("/{id}/friends/{fid}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getFriend(@Context HttpServletRequest request, @PathParam("id") int usrId, @PathParam("fid") int fid)
	{
		if(!Validator.hasPermission(request, new String[]{"AD", "US"}))
			return "{\"status\": 401}";
		
		ObjectMapper om = new ObjectMapper();
		
		try {
					
			return om.writeValueAsString(userDao.prepareUsersForSend(userDao.findFriendOfUser(usrId, fid)));
			
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return "{\"status\": 500}";	
	}
	
	@Path("/{id}/friends/{fid}")
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	public void deleteFriend(@Context HttpServletRequest request, @PathParam("id") int usrId, @PathParam("fid") int fid)
	{
		if(!Validator.hasPermission(request, new String[]{"AD", "US"}))
			return;
		
		try{
			
			friendsDao.findByIdAndRemove(new FriendPK(usrId, fid));
			friendsDao.findByIdAndRemove(new FriendPK(fid, usrId));
			
		}catch (Exception e){
			e.printStackTrace();
		}
	}
	
	
	@Path("/{id}/reservations")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String addReservation(@Context HttpServletRequest request, @PathParam("id") int usrId, String json)
	{
		if(!Validator.hasPermission(request, new String[]{"AD", "US"}))
			return "{\"status\": 401}";
		
		ObjectMapper om = new ObjectMapper();
		
		try {
			
			Reservation res = om.readValue(json, Reservation.class);
			User user = userDao.findByIdAndRole(usrId, "US");
			
			if(user != null)
			{
				res.getId().setUsrId(user.getUsrId());
				//TODO: Proveriti da li je zauzet sto
				res = reservationDao.persist(res);
				System.out.println(""+res.getId().getRsId()+","+res.getId().getTbId()+
						","+res.getId().getRsvSerial()+","+res.getId().getUsrId());
				res = reservationDao.findById(res.getId());
				return om.writeValueAsString(res);
			}
		    
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return "{\"status\": 500}";	
	}
	
	
	@Path("/{id}/reservations")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getReservations(@Context HttpServletRequest request, @PathParam("id") int usrId)
	{
		if(!Validator.hasPermission(request, new String[]{"AD", "US"}))
			return "{\"status\": 401}";
		
		ObjectMapper om = new ObjectMapper();
		
		try {
			
			return om.writeValueAsString(reservationDao.findReservationsForUser(usrId));
			
				    
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return "{\"status\": 500}";	
	}
	
	@Path("/{id}/reservations/{rsId}/{tbId}/{srId}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getReservationById(@Context HttpServletRequest request, 
								 @PathParam("id") int usrId, @PathParam("rsId") int rsId,
								 @PathParam("tbId") int tbId, @PathParam("srId") int srId){
		
		if(!Validator.hasPermission(request, new String[]{"AD", "US"}))
			return "{\"status\": 401}";
		
		ObjectMapper om = new ObjectMapper();
		
		try {
			ReservationPK rpk = new ReservationPK(rsId, usrId, tbId, srId);
			return om.writeValueAsString(reservationDao.findById(rpk));
				    
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return "{\"status\": 500}";	
	}
	
	@Path("/{id}/reservations/{rsId}/{tbId}/{srId}")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String updateReservation(@Context HttpServletRequest request, 
			 @PathParam("id") int usrId, @PathParam("rsId") int rsId,
			 @PathParam("tbId") int tbId, @PathParam("srId") int srId, String json)
	{
		
		if(!Validator.hasPermission(request, new String[]{"AD", "US"}))
			return "{\"status\": 401}";
		
		ObjectMapper om = new ObjectMapper();
		
		try {
			
			Reservation res = om.readValue(json, Reservation.class);
			res.setId(new ReservationPK(rsId, usrId, tbId, srId));
			return om.writeValueAsString(reservationDao.merge(res));
				    
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return "{\"status\": 500}";	
	}
	
	@Path("/{id}/reservations/{rsId}/{tbId}/{srId}")
	@DELETE
	public void deleteReservation(@Context HttpServletRequest request, 
								 @PathParam("id") int usrId, @PathParam("rsId") int rsId,
								 @PathParam("tbId") int tbId, @PathParam("srId") int srId, String json)
	{
		
		if(!Validator.hasPermission(request, new String[]{"AD", "US"}))
			return;
		
		try {
			reservationDao.findByIdAndRemove(new ReservationPK(rsId, usrId, tbId, srId));	    
		}catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	
	@Path("/{id}/invitations")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String addInvitation(@Context HttpServletRequest request, @PathParam("id") int usrId, String json)
	{
		
		if(!Validator.hasPermission(request, new String[]{"AD", "US"}))
			return "{\"status\": 401}";
		
		ObjectMapper om = new ObjectMapper();
		
		try {
			
			Invite inv = om.readValue(json, Invite.class);
			User user = userDao.findByIdAndRole(usrId, "US");
			
			if(user != null)
			{
				inv.getId().setUsrId(user.getUsrId());
				System.out.println("PRVO PRE PERSISTA");
				System.out.println(inv.getId().getFriUsrId());
				System.out.println(inv.getId().getUsrId());
				System.out.println(inv.getId().getRsId());
				System.out.println(inv.getId().getRsvSerial());
				
				inv = inviteDao.persist(inv);
				System.out.println("POSLE PERSISTA");
				System.out.println(inv.getId().getFriUsrId());
				System.out.println(inv.getId().getUsrId());
				System.out.println(inv.getId().getRsId());
				System.out.println(inv.getId().getRsvSerial());
				
				inv = inviteDao.findById(inv.getId());
				System.out.println("POSLE FIND BY");
				System.out.println(inv.getId().getFriUsrId());
				System.out.println(inv.getId().getUsrId());
				System.out.println(inv.getId().getRsId());
				System.out.println(inv.getId().getRsvSerial());
				
				
				inv.setInToken(inv.getId().getRsvSerial()+RandomStringUtils.random(49, true, true));
				inv = inviteDao.merge(inv);
				
				User u = userDao.findById(inv.getId().getFriUsrId());
				
				if(u == null)
					 return "{\"status\":404}";
				
				InvitationInfo info = new InvitationInfo(u.getUsrEmail(), inv.getInToken());
				
				Connection connection = null;
				Session session = null;
				MessageProducer producer = null;
			
				// Creates a connection
				connection = qcf.createConnection();

	            // Creates a session
	            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

	            // Creates a message producer from the Session to the Topic or Queue
	            producer = session.createProducer(mailQueue);

	            // Creates an object message
	            ObjectMessage object = session.createObjectMessage();
	            object.setObject(info);
			    
	            // Tells the producer to send the object message
	            producer.send(object);
	            
	            // Closes the producer
	            producer.close();
	            
	            // Closes the session
	            session.close();
	            
	            // Closes the connection
	            connection.close();			
	            
				return om.writeValueAsString(inv);
			}
		    
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return "{\"status\": 500}";	
	}
	
	
	@Path("/{id}/invitations")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getInvitations(@Context HttpServletRequest request, @PathParam("id") int usrId)
	{
		
		if(!Validator.hasPermission(request, new String[]{"AD", "US"}))
			return "{\"status\": 401}";
		
		ObjectMapper om = new ObjectMapper();
		
		try {
	
			return om.writeValueAsString(inviteDao.findInvitationsForUser(usrId));
				    
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return "{\"status\": 500}";	
	}
	
	@Path("/{id}/invitations/{rsId}/{tbId}/{srId}/{fid}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getInvitationById(@Context HttpServletRequest request, 
								    @PathParam("id") int usrId, @PathParam("rsId") int rsId,
								    @PathParam("tbId") int tbId, @PathParam("srId") int srId,
								    @PathParam("fid") int fid){
		
		if(!Validator.hasPermission(request, new String[]{"AD", "US"}))
			return "{\"status\": 401}";
		
		ObjectMapper om = new ObjectMapper();
		
		try {
			InvitePK ipk = new InvitePK(fid, rsId, usrId, tbId, srId);
			return om.writeValueAsString(inviteDao.findById(ipk));
				    
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return "{\"status\": 500}";	
	}
	
	@Path("/{id}/invitations/{rsId}/{tbId}/{srId}/{fid}")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String updateInvitation(@Context HttpServletRequest request, 
			 @PathParam("id") int usrId, @PathParam("rsId") int rsId,
			 @PathParam("tbId") int tbId, @PathParam("srId") int srId, 
			 @PathParam("fid") int fid, String json)
	{
		
		if(!Validator.hasPermission(request, new String[]{"AD", "US"}))
			return "{\"status\": 401}";
		
		ObjectMapper om = new ObjectMapper();
		
		try {
			
			Invite res = om.readValue(json, Invite.class);
			res.setId(new InvitePK(fid, rsId, usrId, tbId, srId));
			return om.writeValueAsString(inviteDao.merge(res));
				    
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return "{\"status\": 500}";	
	}	
	
	@Path("/invite/conf/")
	@GET
	public Response initeConf(@Context HttpServletResponse response,@QueryParam("token") String token)
	{
		if(token != null)
		{
			Invite i = inviteDao.findByToken(token);
			
			if(i != null)
			{	
				URI location = null;
				try {
					location = new URI("http://localhost:8080/ISAProject/#/user/invite/conf/"+i.getInToken()); 
				} catch (URISyntaxException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				if(location == null)
					return null;
				
				return Response.temporaryRedirect(location).build();
			}
		}
		return null;
	}
	
	@Path("/invitations/token")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getInviteDetails(@QueryParam("token") String token)
	{
		InviteDetails id = new InviteDetails();
		
		ObjectMapper om = new ObjectMapper();
		
		try {
			
			if(token == null || token == "")
				return "{\"status\": 400}";
			System.out.println(token);
			
			Invite inv = inviteDao.findByToken(token);
			if(inv == null)
				return "{\"status\": 400}";
			
			ReservationPK rpk = new ReservationPK(inv.getId().getRsId(), inv.getId().getUsrId(),
					inv.getId().getTbId(), inv.getId().getRsvSerial());
			Reservation res = reservationDao.findById(rpk);
			
			id.date = res.getRsvDate();
			id.time = res.getRsvDuration();
			id.user = userDao.prepareUserForSend(userDao.findById(inv.getId().getUsrId()));
			id.restaurant = restaurantDao.findById(inv.getId().getRsId());
			id.friends = (ArrayList<User>) inviteDao.findFriendsInInvitation(inv);
			id.table = inv.getId().getTbId();
			
			
			return om.writeValueAsString(id);
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "{\"status\": 500}";
	}
	
	
	@Path("/invitations/{token}/{confirm}")
	@POST
	public void confirmInvite(@PathParam("token") String token, @PathParam("confirm") String conf)
	{		
			if(token != null)
			{
				Invite inv = inviteDao.findByToken(token);
				
				if(inv == null)
					return;
				
				if(conf.equals("true"))
					inv.setInConf(true);
				
				inv.setInToken(null);
				
				inviteDao.merge(inv);
			
			}
	}
	
	
	
}
