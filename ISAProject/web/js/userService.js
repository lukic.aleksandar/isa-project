isaProject.service('userService', ['$http', 'messageService', function($http, msgService){
    
    var isFunction = function (fun)
    {
        if(fun != undefined  && (typeof fun === 'function'))
            return true;
        return false;
    }
    
    this.updateUser = function(data, succs, err)
    {   
        
        $http.post('admin/users/'+data.usrId, data)
        
        .success(function(response){
            
            if(response.status == 401 || response.status == 500)
            {
                msgService.logMessage('userService.updateUser', response);
                
                if(isFunction(err))
                    err(response);
                return;
            }
            
            if(isFunction(succs))
            {
                msgService.logMessage('userService.updateUser', response);
                console.log(succs);
                succs(response);
            }
            
        })
        .error(function(response, code){
            
            msgService.logMessage('userService.updateUser', "response: "+response+ "code: " + code);
                
            if(isFunction(err))
                err(response);
        });
        
    }
    
    this.getUserById = function(usrId,succs, err)
    {
        $http.get('admin/users/'+usrId)
        
        .success(function(response){
           
            if(response.status == 401 || response.status == 500)
            {
                msgService.logMessage('userService.getUserById', response);
                
                if(isFunction(err))
                    err(response);
                return;
            }
            
            if(isFunction(succs))
            {
                msgService.logMessage('userService.getUserById', response);
                succs(response);
            }
            
        })
        .error(function(response, code){
            
            msgService.logMessage('userService.getUserById', "response: "+response+ "code: " + code);
                
             if(isFunction(err))
                err(response);
        });
        
    }
      
    this.getFriends = function(usrId,succs, err)
    {
        $http.get('user/'+usrId+'/friends')
        
        .success(function(response){
           
            if(response.status == 401 || response.status == 500)
            {
                msgService.logMessage('userService.getFriends', response);
                
                if(isFunction(err))
                    err(response);
                return;
            }
            
            if(isFunction(succs))
            {
                msgService.logMessage('userService.getFriends', response);
                succs(response);
            }
            
        })
        .error(function(response, code){
            
            msgService.logMessage('userService.getFriends', "response: "+response+ "code: " + code);
                
             if(isFunction(err))
                err(response);
        });
        
    }
    
    this.addFriend = function(usrId, data, succs, err)
    {
        $http.post('user/'+usrId+'/friends', data)
        
        .success(function(response){
            
            if(response.status == 401 || response.status == 500)
            {
                msgService.logMessage('userService.addFriend', response);
                
                 if(isFunction(err))
                    err(response);
                return;
            }
            
            if(isFunction(succs))
            {
                msgService.logMessage('userService.addFriend', response);
                succs(response);
            }
            
        })
        .error(function(response, code){
            
            msgService.logMessage('userService.addFriend', "response: "+response+ "code: " + code);
                
            if(isFunction(err))
                err(response);
        });
        
    }
    
    this.removeFriend = function (usrId, fid, succs, err)
    {
        $http.delete('user/'+usrId+'/friends/'+fid)
            
        .success(function(response){
            
            if(response.status == 401 || response.status == 500)
            {
                msgService.logMessage('userService.removeFriend', response);
                
                if(isFunction(err))
                    err(response);
                return;
            }
            
            if(isFunction(succs))
            {
                msgService.logMessage('userService.removeFriend', response);
                succs(response);
            }
            
        })
        .error(function(response, code){
            
            msgService.logMessage('userService.removeFriend', "response: "+response+ "code: " + code);
                
             if(isFunction(err))
                err(response);
        });   
    }
    
    this.searchUsers = function (string, succs, err)
    {
        $http.get('admin/users/search?search='+string)
        .success(function(response){
            
            if(response.status == 401 || response.status == 500)
            {
                msgService.logMessage('userService.removeFriend', response);
                
                if(isFunction(err))
                    err(response);
                return;
            }
            
            if(isFunction(succs))
            {
                msgService.logMessage('userService.searchUsers', response);
                succs(response);
            }
            
        })
        .error(function(response, code){
            
            msgService.logMessage('userService.searchUsers', "response: "+response+ "code: " + code);
                
             if(isFunction(err))
                err(response);
        });   
    }
    
    
}]);