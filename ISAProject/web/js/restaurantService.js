isaProject.service('restaurantService', ['$http', 'messageService', function ($http, msgService){
    
    var isFunction = function (fun)
    {
        if(fun != undefined  && (typeof fun === 'function'))
            return true;
        return false;
    }
    
    this.addRestaurant = function(data, succs, err)
    {   
        var d = JSON.parse(JSON.stringify(data));
        delete d.rsId;

        $http.post('admin/restaurants', d)
        
        .success(function(response){
            
            if(response.status == 401 || response.status == 500)
            {
                msgService.logMessage('restaurantService.addRestaurant', response);
                
                if(isFunction(err))
                    err(response);
                return;
            }
            
            if(isFunction(succs))
            {
                msgService.logMessage('restaurantService.addRestaurant', response);
                console.log(succs);
                succs(response);
            }
            
        })
        .error(function(response, code){
            
            msgService.logMessage('restaurantService.addRestaurant', "response: "+response+ "code: " + code);
                
            if(isFunction(err))
                err(response);
        });
        
    }
      
    this.getRestaurants = function(succs, err)
    {
        $http.get('admin/restaurants')
        
        .success(function(response){
           
            if(response.status == 401 || response.status == 500)
            {
                msgService.logMessage('restaurantService.getRestaurant', response);
                
                if(isFunction(err))
                    err(response);
                return;
            }
            
            if(isFunction(succs))
            {
                msgService.logMessage('restaurantService.getRestaurant', response);
                succs(response);
            }
            
        })
        .error(function(response, code){
            
            msgService.logMessage('restaurantService.getRestaurant', "response: "+response+ "code: " + code);
                
             if(isFunction(err))
                err(response);
        });
        
    }
    
    this.getRestaurantById = function(id, succs, err)
    {
        $http.get('admin/restaurants/'+id)
        
        .success(function(response){
            
            if(response.status == 401 || response.status == 500)
            {
                msgService.logMessage('restaurantService.getRestaurantById', response);
                
                 if(isFunction(err))
                    err(response);
                return;
            }
            
            if(isFunction(succs))
            {
                msgService.logMessage('restaurantService.getRestaurantById', response);
                succs(response);
            }
            
        })
        .error(function(response, code){
            
            msgService.logMessage('restaurantService.getRestaurantById', "response: "+response+ "code: " + code);
                
            if(isFunction(err))
                err(response);
        });
        
    }
    
    this.updateRestaurant = function (data,succs, err)
    {
        $http.post('admin/restaurants/'+data.rsId, data)
            
        .success(function(response){
            
            if(response.status == 401 || response.status == 500)
            {
                msgService.logMessage('restaurantService.updateRestaurant', response);
                
                if(isFunction(err))
                    err(response);
                return;
            }
            
            if(isFunction(succs))
            {
                msgService.logMessage('restaurantService.addRestaurant', response);
                succs(response);
            }
            
        })
        .error(function(response, code){
            
            msgService.logMessage('restaurantService.updateRestaurant', "response: "+response+ "code: " + code);
                
             if(isFunction(err))
                err(response);
        });
        
    }
    
    this.deleteRestaurant = function (id,succs,err)
    {
        $http.delete('admin/restaurants/'+id)
        
        .success(function(){
            
            if(isFunction(succs))
                succs();
               
        })
        .error(function(response, code){
            
            msgService.logMessage('restaurantService.deleteRestaurant', "response: "+response+ "code: " + code);
                
            if(isFunction(err))
                err(response);
        });
        
    }
    
    
}]);