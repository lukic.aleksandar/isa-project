isaProject.service('loginService', ['$location', '$cookies','$http','$log', 
                   function($location, $cookies, $http, $log){
    
    this.login = function(data, success, error)
    {
        $http.post('auth/login', data)
        .success(function(response){
            
            if(response.usrId != undefined)
            {
                $cookies.putObject("user", response);
                if(success!=undefined)
                    success();
            }else{
                if(error!=undefined)
                    error(response, undefined);
            }
        })
        .error(function(response, code){
             if(error!=undefined)
                    error(response,code);
        });
    }
    
    this.logout = function(success, error)
    {
        $http.post('auth/logout','{}')
        .success(function(response){
            
            if(response.status == 200)
            {
                $cookies.remove("user");
                if(success != undefined)
                    success();
            }else{
                if(error!=undefined)
                    error(response, undefined);
            }
        })
        .error(function(response, code){
             if(error!=undefined)
                    error(response, code);
        });
    }
    
    this.register = function(data, success, error)
    {
        $http.post('auth/register', data)
        .success(function(response){
            
            if(response.status == 200)
            {
                if(success != undefined)
                    success();
            }else{
                if(error!=undefined)
                    error(response, undefined);
            }
        })
        .error(function(response, code){
            if(error!=undefined)
                    error(response, code);
        });
    }
}]);