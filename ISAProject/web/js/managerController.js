isaProject.controller('managerController', ['$scope','$location','loginService','menuService','messageService','restaurantService','managerService', 
                                          function($scope,$location ,logService, menuService, msgService, restService, mngService){
    
                                              
    $scope.tableInputValid = true;
    $scope.errMessage = "";
                                              
    $scope.menuInput = {
        id: {
            rsId: "",
            miId: ""
        },
        miName : "",
        miPrice: "",
        miDesc: ""
    }
    
    $scope.restaurantInput = {
        rsId: "", 
        rsName: "",
        rsTown: "",
        rsAddress:"",
        rsDesc: "",
        rsAvggrade: ""
    }
    
    $scope.restaurant = {
        rsId: "", 
        rsName: "",
        rsTown: "",
        rsAddress:"",
        rsDesc: "",
        rsAvggrade: ""
    }
  
    $scope.state = 'none';
    $scope.menuItems = [];
    $scope.tabels = [];
    $scope.tabelMap = [];
    $scope.tabelState = '';
    
    $scope.currentCell = {
        tbRow : '', 
        tbCol: '', 
        tbSeats: '', 
        id: {
            tbId: '',
            rsId: $scope.restaurant.rsId
        }
    }
    
    
    $scope.addMenuItem = function ()
    {   
        
        if(!logService.hasPermission(['MN']))
            return;
        
        var succs = function(data){
             $scope.menuItems.push(data);   
             $scope.initMenuInput();
        }

        var err = function(){}
        
        var user = logService.getLoggedUser();
        
        if(user.restaurant != undefined)
        {
            $scope.menuInput.id.rsId = user.restaurant.rsId;
            menuService.addMenuItem($scope.menuInput, succs, err);
        }
    }
    
    $scope.getMenuItems = function()
    {
        if(!logService.hasPermission(['MN']))
            return;
        
        var succs = function(data){
             $scope.menuItems = data;     
        }

        var err = function(){}
        
        var user = logService.getLoggedUser();
        
        if(user.restaurant != undefined)
        {
             $scope.menuInput.id.rsId = user.restaurant.rsId;
             menuService.getMenuItems(user.restaurant.rsId, succs, err);
        }
    }
    
    $scope.getMenuItemById = function()
    {
        if(!logService.hasPermission(['MN']))
            return;
        
        var succs = function(data){
             $scope.menuItemTemp = data;
        }

        var err = function(){}
        
        var user = logService.getLoggedUser();
        
        if(user.restaurant != undefined)
        {   
            $scope.menuInput.id.rsId = user.restaurant.rsId;
            menuService.getMenuItemById($scope.menuInput.id.rsId, $scope.menuInput.id.miId, succs, err);
        }
    }
    
    $scope.updateMenuItem = function()
    {
        if(!logService.hasPermission(['MN']))
            return;
        
        var succs = function(data){
             
            for(var i = 0; i < $scope.menuItems.length; i++)
                if($scope.menuItems[i].id.rsId === data.id.rsId 
                   && $scope.menuItems[i].id.miId === data.id.miId)
                    $scope.menuItems[i] = data
            $scope.state = 'none';
            $scope.initMenuInput();
        }

        var err = function(){}
        
        var user = logService.getLoggedUser();
        
        if(user.restaurant != undefined)
        {   
            $scope.menuInput.id.rsId = user.restaurant.rsId;
            menuService.updateMenuItem($scope.menuInput, succs, err);
        }
    }
    
    $scope.deleteMenuItem = function(index)
    {
        if(!logService.hasPermission(['MN']))
            return;
        
        var succs = function(){
            $scope.menuItems.splice(index, 1);
        }

        var err = function(){}
        
        var user = logService.getLoggedUser();
        if(user.restaurant != undefined)
        {   
            $scope.menuItems[index].id.rsId = user.restaurant.rsId;
            menuService.deleteMenuItem($scope.menuItems[index].id.rsId, $scope.menuItems[index].id.miId, succs, err);
        }
    }
    
    
    $scope.addTabels = function ()
    {   
        $scope.tableInputValid = true;
        $scope.errMessage = "";
        $scope.tabels = [];
        for(var i = 0; i < 6; i++)
        {
            for(var j = 0; j < 6; j++)
            {   
                if($scope.tabelMap[i][j].id.tbId != "" && $scope.tabelMap[i][j].tbSeats != ""){
                     $scope.tabels.push($scope.tabelMap[i][j]);
                }else if ($scope.tabelMap[i][j].id.tbId != "" && $scope.tabelMap[i][j].tbSeats == ""){
                    
                    $scope.tableInputValid = false;
                    $scope.errMessage = "Pokušavate da dodate sto bez broja mesta! Oznaka stola: "+$scope.tabelMap[i][j].tbId;
                    $scope.tabels = [];
                    return;
                    
                }else if ($scope.tabelMap[i][j].id.tbId == "" && $scope.tabelMap[i][j].tbSeats != ""){
                    $scope.tableInputValid = false;
                    $scope.errMessage = "Pokušavate da dodate sto bez oznake! stavili ste samo broj mesta!";
                    $scope.tabels = [];
                    return;
                }
                
            }
        }
        
        for(var i = 0; i < $scope.tabels.length-1; i++)
        {
            for( var j = i+1; j < $scope.tabels.length; j++)
            {
                if($scope.tabels[i].id.tbId == $scope.tabels[j].id.tbId)
                {
                     $scope.tableInputValid = false;
                     $scope.errMessage = "Pokušavate da dodate dva stola sa istom oznakom!";
                     return;
                }
            }
        }
        
        
        var succs = function (resp){ console.log(resp)}
        var err = function(){}
        
        mngService.addTabels($scope.restaurant.rsId, $scope.tabels, succs,err);
        $scope.tabelState = 'defined';
    }
    
    
    $scope.prepareMenuItemUpdate = function (index)
    {
        var menuItem = $scope.menuItems[index];
        
        var user = logService.getLoggedUser();
        if(user.restaurant != undefined)
        {    
            menuItem.id.rsId = user.restaurant.rsId;
            $scope.menuInput.id.miId = menuItem.id.miId;
            $scope.menuInput.id.rsId = menuItem.id.rsId;
            $scope.menuInput.miDesc = menuItem.miDesc;
            $scope.menuInput.miName = menuItem.miName;
            $scope.menuInput.miPrice = menuItem.miPrice;

            $scope.state = 'update';
        }
    }
    
    $scope.updateRestaurant = function()
    {
        if(!logService.hasPermission(['MN']))
            return;
        
        var succs = function(data){
            $scope.restaurant = data;
            $scope.state = 'none';
            $scope.initRestaurantInput();
        }

        var err = function(){}

        restService.updateRestaurant($scope.restaurantInput, succs, err);
    }
    
    $scope.cancelUpdate = function()
    {
        $scope.state = 'none';
        $scope.initMenuInput();
        $scope.initRestaurantInput();
    }
    
    $scope.initMenuInput = function()
    {
        $scope.menuInput = {
             id: {
                rsId: "",
                miId: ""
            },
            miName : "",
            miPrice: "",
            miDesc: ""
        }
    }
    
    $scope.initRestaurantInput = function()
    {
        $scope.restaurantInput = {
            rsId: "", 
            rsName: "",
            rsTown: "",
            rsAddress:"",
            rsDesc: "",
            rsAvggrade: ""
        }
    }
    
    $scope.prepareRestaurantUpdate = function ()
    {       
        $scope.restaurantInput.rsId = $scope.restaurant.rsId; 
        $scope.restaurantInput.rsName = $scope.restaurant.rsName;
        $scope.restaurantInput.rsTown = $scope.restaurant.rsTown;
        $scope.restaurantInput.rsAddress = $scope.restaurant.rsAddress;
        $scope.restaurantInput.rsDesc = $scope.restaurant.rsDesc;
        
        $scope.state = 'update';
    }
    
    $scope.setCurrentCell = function(cell){
        
        $scope.currentCell = cell;
    }
    
    $scope.editTableCell = function()
    {
       $scope.tabelMap[$scope.currentCell.tbRow][$scope.currentCell.tbCol] = $scope.currentCell;
       console.log($scope.tabelMap[$scope.currentCell.tbRow][$scope.currentCell.tbCol]);
       console.log($scope.tabelMap);
       $scope.initCurrentCell();
    }
    
    $scope.cancelTableEdit = function()
    {
        $scope.initCurrentCell();
    }
    
    $scope.createTableMap = function()
    {
        for (var i = 0; i < 6; i++) {
            var row = [];
            
            for (var j = 0; j < 6; j++) {
              row.push({tbRow : i, tbCol: j, tbSeats: '', id: {tbId: '', rsId: $scope.restaurant.rsId }});
            }
            $scope.tabelMap.push(row);
          }
    }
    
    $scope.initCurrentCell = function()
    {
        $scope.currentCell = {
            tbRow : "", 
            tbCol: "", 
            tbSeats: '', 
            id: {
                tbId: '',
                rsId: ''
            }
        }
    }
    
    $scope.getTabels = function()
    {
        if(!logService.hasPermission(['MN']))
            return;
        
        var succs = function(data){
           if(data.length > 0){
               
               $scope.tabelState = 'defined'; 
               
               for (var k = 0; k < data.length; k++)
               {
                    for (var i = 0; i < 6; i++) {
                        for (var j = 0; j < 6; j++) {
                            if(data[k].tbCol == $scope.tabelMap[i][j].tbCol && data[k].tbRow == $scope.tabelMap[i][j].tbRow )
                                $scope.tabelMap[i][j] = data[k];
                        }
                    }
               }
               
           }else 
               $scope.tabelState = 'undefined';
        }

        var err = function(){}

        mngService.getTabels($scope.restaurant.rsId, succs, err);
    }
    
    var callback = function()
    {   
        
        if(!logService.hasPermission(['MN']))
                return;
        
      
        var user = logService.getLoggedUser();
        
        if(user.restaurant == undefined)
            return;
       
        if($location.path() === '/manager/menu')
            $scope.getMenuItems();

        $scope.restaurant = user.restaurant;   
        $scope.createTableMap();
        
         if($location.path() ==='/manager/tabels')
            $scope.getTabels();
        
    }
    
    logService.stillLoggedIn(callback);
   
   
    
   
    
   
}]);
