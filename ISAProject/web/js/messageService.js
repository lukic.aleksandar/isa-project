isaProject.service('messageService', ['$log', function ($log){
    
    this.logMessage = function(sender, msg)
    {
        $log.info('##################');
        $log.info('Sender: ' + sender);
        $log.info('Message: ' + msg );
        $log.info('##################');
    }
    
    
}]);