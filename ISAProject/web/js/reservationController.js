isaProject.controller('reservationController', ['$scope','$location','userService','restaurantService','loginService'
                                                ,'managerService','reservationService','inviteService',
                                                function ($scope, $location, usrService, restService, logService, mngService, reserService,inviteSerice){
  
    $scope.user = {
        usrId: "",
        usrEmail : "",
        usrPass : "",
        usrName: "",
        usrSurname: "",
        usrTown: "",
        usrAddress: "",
        usrVisits: ""
    }
    
    $scope.reservationInput =
    {
        id: {
            usrId: "",
            rsId: "",
            tbId: "",
        },  
        rsvDate:"",
        rsvDuration:"",
        rsvStatus: ""
    }
    
    $scope.reservationInvites = [];
    $scope.searchInput = "";
    $scope.restaurants = [];
    $scope.friends = [];
    $scope.reservationStatus = "";
    $scope.currentRestaurant = {
        rsId: "",
        rsName: ""
    }
    
    $scope.currentCell = {
            tbRow : "", 
            tbCol: "", 
            tbSeats: '', 
            id: {
                tbId: '',
                rsId: ''
            }
    }
    
    $scope.tabelMap = []
    
    
    $scope.getRestaurants = function ()
    {   
        
        if(!logService.hasPermission(['US']))
            return;
        
        var succs = function(data){
             $scope.restaurants = data; 
             angular.forEach($scope.restaurants, function(member, index){
                  member.index = index;
             }); 
        }

        var err = function(){}
        restService.getRestaurants( succs, err);
            
    }
    
    $scope.getFriends = function(callback)
    {
        if(!logService.hasPermission(['US']))
            return;
        
        var succs = function(data){
             $scope.friends = data;  
            if(callback != undefined)
                callback();
        }

        var err = function(){}

        usrService.getFriends($scope.user.usrId,succs, err);
    }
    
    $scope.inviteFriend = function(index){
        
        $scope.reservationInvites.push($scope.friends[index]);
        $scope.friends.splice(index,1);
    }
    
    $scope.uninviteFriend = function(index)
    {   
         $scope.friends.push($scope.reservationInvites[index]);
         $scope.reservationInvites.splice(index,1);
    }
    
    $scope.startReservation = function(index)
    {
        if(!logService.hasPermission(['US']))
            return;
        $scope.cancelReservation();
        $scope.reservationInput.id.rsId = $scope.restaurants[index].rsId;
        $scope.reservationInput.id.usrId = $scope.user.usrId;
        $scope.reservationStatus = 'selectDT';
        $scope.currentRestaurant = $scope.restaurants[index];
        
    }
    
    $scope.reservationTableStage = function()
    {
        if(!logService.hasPermission(['US']))
            return;
        
        $scope.getTabels();
        
    }
    
    $scope.reservationFriendStage = function()
    {   
         if(!logService.hasPermission(['US']))
            return;
         $scope.reservationInput.id.tbId = $scope.currentCell.id.tbId;
         $scope.reservationStatus = 'selectFr';
    }    
    
    $scope.reservationConfirmStage = function(){
        
         if(!logService.hasPermission(['US']))
            return;
         $scope.reservationStatus = 'confirm';
    }
    
    $scope.prevStage = function()
    {
        if($scope.reservationStatus === 'confirm')
            $scope.reservationStatus = 'selectFr';
        else if ($scope.reservationStatus === 'selectFr')
            $scope.reservationStatus = 'selectTB';
        else if ($scope.reservationStatus === 'selectTB')
            $scope.reservationStatus = 'selectDT';
    }
    
    $scope.createReservation = function()
    {
        if(!logService.hasPermission(['US']))
            return;
        
        var succs = function(data){
            
            
            var s = function (){
                 $scope.reservationStatus = 'msgConf';
                 $scope.cancelReservation();
            }
            
            var e = function(){
                 $scope.reservationStatus = 'msgDen';
                 $scope.cancelReservation();
            }
            
            var inviteArr = [];
            
            for (var i = 0; i < $scope.reservationInvites.length; i++)
            {
                var invite = {
                    id:{
                        friUsrId: $scope.reservationInvites[i].usrId,
                        rsId: data.id.rsId,
                        usrId: data.id.usrId,
                        tbId: data.id.tbId,
                        rsvSerial: data.id.rsvSerial
                    }
                }
                
                inviteArr.push(invite);
            }
            
            inviteSerice.createInvites($scope.user.usrId,inviteArr, s, e);    
        }

        var err = function(){
            $scope.cancelReservation();
            $scope.reservationStatus = 'msgDen';
        }
        
        $scope.reservationInput.rsvStatus = 1;
        reserService.createReservation($scope.reservationInput.id.usrId, $scope.reservationInput, succs, err);
    }
    
    
    $scope.cancelReservation = function ()
    {
        $scope.initReservationInput();
        
         for (var i = 0; i < $scope.reservationInvites.length; i++)
                $scope.friends.push($scope.reservationInvites[i]);
        
        
        $scope.reservationInvites = [];
        $scope.createTableMap();
        $scope.initCurrentCell();
    }
    
    $scope.initReservationInput = function()
    {
        $scope.reservationInput =
        {
            id: {
                usrId: "",
                rsId: "",
                tbId: "",
                rsvSerial: ""
            },  
            rsvDate:"",
            rsvDuration:"",
            rsvStatus: ""
        }
        
        $scope.currentRestaurant = {
            rsId: "",
            rsName:  ""
        }   
    }
    
    
    $scope.createTableMap = function()
    {
        $scope.tabelMap = [];
      
        for (var i = 0; i < 6; i++) {
            var row = [];
            
            for (var j = 0; j < 6; j++) {
              row.push({tbRow : i, tbCol: j, tbSeats: '', id: {tbId: '', rsId: '' }});
            }
            $scope.tabelMap.push(row);
          }
    }
    
    $scope.initCurrentCell = function()
    {
        $scope.currentCell = {
            tbRow : "", 
            tbCol: "", 
            tbSeats: '', 
            id: {
                tbId: '',
                rsId: ''
            }
        }
    }
    
    $scope.setCurrentCell = function(col)
    {
        if (col.tbId != "" && col.tbStatus == false)
        {
            $scope.currentCell = col;
        }
    }
    
    $scope.isSelected = function(col)
    {
        if($scope.currentCell.tbCol == col.tbCol && $scope.currentCell.tbRow == col.tbRow
           && $scope.currentCell.id.tbId != "")
            return true;
        return false;
    }
    
    $scope.getTabels = function()
    {
        if(!logService.hasPermission(['US']))
            return;
        
        var succs = function(data){
           if(data.length > 0){
               
               for (var k = 0; k < data.length; k++)
               {
                    for (var i = 0; i < 6; i++) {
                        for (var j = 0; j < 6; j++) {
                            if(data[k].tbCol == $scope.tabelMap[i][j].tbCol && data[k].tbRow == $scope.tabelMap[i][j].tbRow )
                                $scope.tabelMap[i][j] = data[k];
                        }
                    }
               }  
               
            $scope.reservationStatus = 'selectTB';
           }
        }

        var err = function(){}

        mngService.getTabelsStatus($scope.currentRestaurant.rsId,new Date($scope.reservationInput.rsvDate).getTime(),$scope.reservationInput.rsvDuration,succs, err);
    }
    
    
    
  
    var callback = function()
    {
        if(!logService.hasPermission(['US']))
            return;                                             
        $scope.user = logService.getLoggedUser();
        $scope.createTableMap();
        $scope.getRestaurants();                                     
        $scope.getFriends();
    }
    
    logService.stillLoggedIn(callback);
    
  
    
}]);