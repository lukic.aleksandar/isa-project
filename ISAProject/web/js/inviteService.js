isaProject.service('inviteService', ['$http', 'messageService', function($http, msgService){
    
    var isFunction = function (fun)
    {
        if(fun != undefined  && (typeof fun === 'function'))
            return true;
        return false;
    }
    
    this.createInvites = function(usrId,data, succs, err)
    {   
        for (var i = 0; i < data.length; i++)
        {
            data = JSON.parse(JSON.stringify(data));
       
            $http.post('user/'+usrId+'/invitations/', data[i])

            .success(function(response){

                if(response.status == 401 || response.status == 500)
                {
                    msgService.logMessage('inviteService.createReservation', response);

                    if(isFunction(err))
                        err(response);
                    return;
                }

                if(isFunction(succs))
                {
                    msgService.logMessage('inviteService.createReservation', response);
                    succs(response);
                }

            })
            .error(function(response, code){

                msgService.logMessage('inviteService.createReservation', "response: "+response+ "code: " + code);

                if(isFunction(err))
                    err(response);
            });
            
        }
        
    }
    
    
    this.getInvitations = function(usrId, succs, err)
    {   
        $http.get('user/'+usrId+'/invitations/')
        
        .success(function(response){
            
            if(response.status == 401 || response.status == 500)
            {
                msgService.logMessage('inviteService.createReservation', response);
                
                if(isFunction(err))
                    err(response);
                return;
            }
            
            if(isFunction(succs))
            {
                msgService.logMessage('inviteService.createReservation', response);
                succs(response);
            }
            
        })
        .error(function(response, code){
            
            msgService.logMessage('inviteService.createReservation', "response: "+response+ "code: " + code);
                
            if(isFunction(err))
                err(response);
        });
        
    }
    
    
    // NIJE URADJENO
    this.updateReservation = function(usrId,data, succs, err)
    {   
        data = JSON.parse(JSON.stringify(data));
        delete data.id.rsvSerial;
        //OVO NIJE GOTOVO
        $http.post('user/'+usrId+'/reservations/'+data.id.rsId, data)
        
        .success(function(response){
            
            if(response.status == 401 || response.status == 500)
            {
                msgService.logMessage('inviteService.createReservation', response);
                
                if(isFunction(err))
                    err(response);
                return;
            }
            
            if(isFunction(succs))
            {
                msgService.logMessage('inviteService.createReservation', response);
                succs(response);
            }
            
        })
        .error(function(response, code){
            
            msgService.logMessage('inviteService.createReservation', "response: "+response+ "code: " + code);
                
            if(isFunction(err))
                err(response);
        });
        
    }
    
    
    this.getInvitationDetailsByToken = function(token,succs,err)
    {   
        console.log("TOKEN");
         console.log(token);
         $http.get('user/invitations/token', {params:{'token': token.token}})
        
        .success(function(response){
            
            if(response == undefined || response.status == 400 || response.status == 500)
            {
                msgService.logMessage('inviteService.getInvitationDetailsByToken', response);
                
                if(isFunction(err))
                    err(response);
                return;
            }
            
            if(isFunction(succs))
            {
                msgService.logMessage('inviteService.getInvitationDetailsByToken', response);
                succs(response);
            }
            
        })
        .error(function(response, code){
            
            msgService.logMessage('inviteService.createReservation', "response: "+response+ "code: " + code);
                
            if(isFunction(err))
                err(response);
        });
    }
    
    this.confirmInvite = function(token, answer, err)
    {
        $http.post('user/invitations/'+token.token+'/'+answer)
        
        .error(function(response, code){
            
            msgService.logMessage('inviteService.createReservation', "response: "+response+ "code: " + code);
                
            if(isFunction(err))
                err(response);
        });
    }
    
    
    
}]);