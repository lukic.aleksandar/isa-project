isaProject.service('managerService', ['$http', 'messageService', function($http,  msgService){
    
    var isFunction = function (fun)
    {
        if(fun != undefined  && (typeof fun === 'function'))
            return true;
        return false;
    }
    
    this.addManager = function(data, succs, err)
    {   
        var d = JSON.parse(JSON.stringify(data));
        delete d.usrId;

        $http.post('admin/managers', d)
        
        .success(function(response){
            
            if(response.status == 401 || response.status == 500)
            {
                msgService.logMessage('managerService.addManager', response);
                
                if(isFunction(err))
                    err(response);
                return;
            }
            
            if(isFunction(succs))
            {
                msgService.logMessage('managerService.addManager', response);
                console.log(succs);
                succs(response);
            }
            
        })
        .error(function(response, code){
            
            msgService.logMessage('managerService.addManager', "response: "+response+ "code: " + code);
                
            if(isFunction(err))
                err(response);
        });
        
    }
      
    this.getManagers = function(succs, err)
    {
        $http.get('admin/managers')
        
        .success(function(response){
            console.log("adasdasdas")
            console.log(isFunction);
            if(response.status == 401 || response.status == 500)
            {
                msgService.logMessage('managerService.getManagers', response);
                
                if(isFunction(err))
                    err(response);
                return;
            }
            
            if(isFunction(succs))
            {
                msgService.logMessage('managerService.getManagers', response);
                succs(response);
            }
            
        })
        .error(function(response, code){
            
            msgService.logMessage('managerService.getManagers', "response: "+response+ "code: " + code);
                
             if(isFunction(err))
                err(response);
        });
        
    }
    
    this.getManagerById = function(id, succs, err)
    {
        $http.get('admin/managers/'+id)
        
        .success(function(response){
            
            if(response.status == 401 || response.status == 500)
            {
                msgService.logMessage('managerService.getManagerById', response);
                
                 if(isFunction(err))
                    err(response);
                return;
            }
            
            if(isFunction(succs))
            {
                msgService.logMessage('managerService.getManagerById', response);
                succs(response);
            }
            
        })
        .error(function(response, code){
            
            msgService.logMessage('managerService.getManagerById', "response: "+response+ "code: " + code);
                
            if(isFunction(err))
                err(response);
        });
        
    }
    
    this.updateManager = function (data,succs, err)
    {
        $http.post('admin/managers/'+data.usrId, data)
            
        .success(function(response){
            
            if(response.status == 401 || response.status == 500)
            {
                msgService.logMessage('managerService.updateManager', response);
                
                if(isFunction(err))
                    err(response);
                return;
            }
            
            if(isFunction(succs))
            {
                msgService.logMessage('managerService.addManager', response);
                succs(response);
            }
            
        })
        .error(function(response, code){
            
            msgService.logMessage('managerService.updateManager', "response: "+response+ "code: " + code);
                
             if(isFunction(err))
                err(response);
        });
        
    }
    
    this.deleteManager = function (id,succs,err)
    {
        $http.delete('admin/managers/'+id)
        
        .success(function(){
            
            if(isFunction(succs))
                succs();
               
        })
        .error(function(response, code){
            
            msgService.logMessage('managerService.deleteManager', "response: "+response+ "code: " + code);
                
            if(isFunction(err))
                err(response);
        });
        
    }
    
    this.addTabels = function(rsId,tabels, succs, err)
    {
        for(var i = 0; i < tabels.length; i++)
        {
            $http.post('manager/'+rsId+'/tables/',tabels[i])
            
            .success(function(response){

                if(response.status == 401 || response.status == 500)
                {
                    msgService.logMessage('managerService.getTabels', response);

                    if(isFunction(err))
                        err(response);
                    return;
                }

                if(isFunction(succs))
                {
                    msgService.logMessage('managerService.getTabels', response);
                    succs(response);
                }

            })
            .error(function(response, code){
            
                msgService.logMessage('managerService.getTabels', "response: "+response+ "code: " + code);

                if(isFunction(err))
                    err(response);
            });
        }
    }
    
    this.getTabels = function(rsId, succs, err)
    {
        $http.get('manager/'+rsId+'/tables/')
        
        .success(function(response){
            
            if(response.status == 401 || response.status == 500)
            {
                msgService.logMessage('managerService.getTabels', response);
                
                if(isFunction(err))
                    err(response);
                return;
            }
            
            if(isFunction(succs))
            {
                msgService.logMessage('managerService.getTabels', response);
                succs(response);
            }
               
        })
        .error(function(response, code){
            
            msgService.logMessage('managerService.getTabels', "response: "+response+ "code: " + code);
                
            if(isFunction(err))
                err(response);
        });
    }
    
    
    this.getTabelsStatus = function(rsId, date, time, succs, err)
    {
        $http.get('manager/'+rsId+'/tables/status', {params: {'dt': date, 't': time}})
        
        .success(function(response){
            
            if(response.status == 401 || response.status == 500)
            {
                msgService.logMessage('managerService.getTabelsStatus', response);
                
                if(isFunction(err))
                    err(response);
                return;
            }
            
            if(isFunction(succs))
            {
                msgService.logMessage('managerService.getTabelsStatus', response);
                succs(response);
            }
               
        })
        .error(function(response, code){
            
            msgService.logMessage('managerService.getTabelsStatus', "response: "+response+ "code: " + code);
                
            if(isFunction(err))
                err(response);
        });
    }
    
    
    
        
}]);