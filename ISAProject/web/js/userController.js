isaProject.controller('userController', ['$scope','$location','userService','restaurantService','loginService',
                                         function ($scope, $location, usrService, restService, logService){

    $scope.userInput = {
        usrId: "",
        usrEmail : "",
        usrPass : "",
        usrName: "",
        usrSurname: "",
        usrTown: "",
        usrAddress: ""
    }
    
    $scope.user = {
        usrId: "",
        usrEmail : "",
        usrPass : "",
        usrName: "",
        usrSurname: "",
        usrTown: "",
        usrAddress: "",
        usrVisits: ""
    }
    
    $scope.searchResult = []
    $scope.searchInput = "";
    $scope.friends = []
    $scope.searchStatus = "";
    
    $scope.updateUser = function ()
    {   
        
        if(!logService.hasPermission(['US']))
            return;
        
        var succs = function(data){
             $scope.user = data;   
             $scope.initUserInput();
             $scope.state = 'none';
        }

        var err = function(){}
        usrService.updateUser($scope.userInput, succs, err);
            
    }
    
    $scope.getFriends = function()
    {
        if(!logService.hasPermission(['US']))
            return;
        
        var succs = function(data){
             $scope.friends = data;  
            console.log(data);
        }

        var err = function(){}

        usrService.getFriends($scope.user.usrId,succs, err);
    }
    
    $scope.addFriend = function(index)
    {
        if(!logService.hasPermission(['US']))
            return;
        
        var succs = function(data){
            
            var s = function (resp)
            {
                 $scope.friends.push(resp);
                 $scope.searchResult.splice(index,1);
            }
            
            var e = function() {}
           
            usrService.getUserById(data.id.friendUsrId, s, e);
           
        }

        var err = function(){}

        usrService.addFriend($scope.user.usrId, $scope.searchResult[index], succs, err);
    }
    
    $scope.removeFriend = function(index)
    {
        if(!logService.hasPermission(['US']))
            return;
        
        var succs = function(data){
             $scope.friends.splice(index,1);
        }

        var err = function(){}

        usrService.removeFriend($scope.user.usrId, $scope.friends[index].usrId, succs, err);
    }
    
    $scope.searchUsers = function ()
    {
        if(!logService.hasPermission(['US']))
            return;
        
        var succs = function(data){
             $scope.searchResult = data;
             $scope.searchStatus = 'finished';
        }
        var err = function(){}
        
        if($scope.searchInput != "")
            usrService.searchUsers($scope.searchInput, succs, err);
    }
    
    $scope.prepareUserUpdate = function ()
    {
        var user = $scope.user;
        
        $scope.userInput.usrAddress = user.usrAddress;
        $scope.userInput.usrTown = user.usrTown;
        $scope.userInput.usrId = user.usrId;
        $scope.userInput.usrName = user.usrName;
        $scope.userInput.usrSurname = user.usrSurname;
        $scope.userInput.usrPass = user.usrPass;
        $scope.userInput.usrEmail = user.usrEmail;
        
        $scope.state = 'update';
    }
    
    $scope.cancelSearch = function ()
    {
        $scope.searchInput = "";
        $scope.searchResult = [];
        $scope.searchStatus = "";
    }
    
    $scope.initUserInput = function()
    {
        $scope.userInput = {
            usrId: "",
            usrEmail : "",
            usrPass : "",
            usrName: "",
            usrSurname: "",
            usrTown: "",
            usrAddress: ""
        }
    }
    
    $scope.isFriend = function(index)
    {
        var user = $scope.searchResult[index];
        
        for (var i = 0; i < $scope.friends.length; i++)
            if(user.usrId == $scope.friends[i].usrId)
                return true;
        
    }
    
    $scope.cancelUpdate = function()
    {
        $scope.state = 'none';
        $scope.searchStatus = "";
        $scope.initUserInput();
    }
    
    var callback = function()
    {
        if(!logService.hasPermission(['US']))
            return;                                             
        $scope.user = logService.getLoggedUser();
                                             
        if($location.path() !== 'user/home')
            $scope.getFriends();
    }
    
    logService.stillLoggedIn(callback);
    
  
    
}]);