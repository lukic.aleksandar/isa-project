isaProject.controller('loginController', ['$scope', '$log', '$location','$cookies','loginService',
                                          function($scope, $log, $location, $cookies, loginService){
    
    $scope.userInput = {
        usrEmail: "",
        usrName: "",
        usrSurname: "",
        usrPass: "",
        usrPassRpt: ""
    }
    
    $scope.inputValid = {
        msg : "",
        valid : true
    }
    
    $scope.login = function ()
    {   
        var success = function()
        {
            $location.path('/');
        }
        
        var error = function(response, code)
        {
            $log.error(response);
           
            if( code!= undefined )
                $log.error(code);
        }
      
        
        data = JSON.parse(JSON.stringify($scope.userInput))
        delete data.usrPassRpt;
        loginService.login(data, success, error);
    }
    
    $scope.logout = function()
    {
        var success = function()
        {
            $location.path('/login');
        }
        
        var error = function(response, code)
        {
            $log.error(response);
           
            if( code!= undefined )
                $log.error(code);
        }
            
        loginService.logout(success, error);
    }
    
    $scope.register = function()
    {
         var success = function()
        {
            $location.path('/afRegister');
        }
        
        var error = function(response, code)
        {
            $log.error(response);
           
            if( code!= undefined )
                $log.error(code);
        }
        
        if($scope.userInput.usrPass === $scope.userInput.usrPassRpt 
        && $scope.userInput.usrPass!=undefined &&  $scope.userInput.usrPass!= "")
        {
             data = JSON.parse(JSON.stringify($scope.userInput))
             delete data.usrPassRpt;
            
            $scope.inputValid.msg = "";
            $scope.inputValid.valid = true;
            
            loginService.register(data, success, error);
        }else{
            
            $scope.inputValid.msg = "Unete šifre u dva polja se razlikuju!";
            $scope.inputValid.valid = false;
        }
        
       
    }
    
    $scope.getLoggedUser = function()
    {
        var user = $cookies.getObject('user');
       /* $log.info("getLoggedUser");
        console.log(user);
        $log.info("##########");
        */
        if(user == undefined)
        { 
            return "";
        }else{   
            user.usrName = user.usrName || "";
            user.usrSurname = user.usrSurname || "";
            return user.usrName +" "+ user.usrSurname;
        }
    }
   
    var callback = function()
    {  
        if($location.path() === '/')
        {
            var user = loginService.getLoggedUser();
            
            if(user == null)
            {
                $location.path('/login');
            }else{

                if(user.usrRole === 'AD')
                    $location.path('/admin/restaurants');
                else if (user.usrRole === 'MN')
                    $location.path('/manager/restaurant');
                else if (user.usrRole === 'US')
                    $location.path('/user/home');   
            }
        }
    }
    loginService.stillLoggedIn(callback);
    
}]);