isaProject.service('menuService', ['$http', 'messageService', function($http, msgService){
    
    var isFunction = function (fun)
    {
        if(fun != undefined  && (typeof fun === 'function'))
            return true;
        return false;
    }
    
    this.addMenuItem = function(data, succs, err)
    {   
        
        $http.post('manager/'+data.id.rsId+'/menuitems', data)
        
        .success(function(response){
            
            if(response.status == 401 || response.status == 500)
            {
                msgService.logMessage('menuService.addMenuItem', response);
                
                if(isFunction(err))
                    err(response);
                return;
            }
            
            if(isFunction(succs))
            {
                msgService.logMessage('menuService.addMenuItem', response);
                console.log(succs);
                succs(response);
            }
            
        })
        .error(function(response, code){
            
            msgService.logMessage('menuService.addMenuItem', "response: "+response+ "code: " + code);
                
            if(isFunction(err))
                err(response);
        });
        
    }
      
    this.getMenuItems = function(rsId,succs, err)
    {
        $http.get('manager/'+rsId+'/menuitems')
        
        .success(function(response){
           
            if(response.status == 401 || response.status == 500)
            {
                msgService.logMessage('menuService.getMenuItems', response);
                
                if(isFunction(err))
                    err(response);
                return;
            }
            
            if(isFunction(succs))
            {
                msgService.logMessage('menuService.getMenuItems', response);
                succs(response);
            }
            
        })
        .error(function(response, code){
            
            msgService.logMessage('menuService.getMenuItems', "response: "+response+ "code: " + code);
                
             if(isFunction(err))
                err(response);
        });
        
    }
    
    this.getMenuItemById = function(rsId, id, succs, err)
    {
        $http.get('manager/'+rsId+'/menuitems/'+id)
        
        .success(function(response){
            
            if(response.status == 401 || response.status == 500)
            {
                msgService.logMessage('menuService.getMenuItemById', response);
                
                 if(isFunction(err))
                    err(response);
                return;
            }
            
            if(isFunction(succs))
            {
                msgService.logMessage('menuService.getMenuItemById', response);
                succs(response);
            }
            
        })
        .error(function(response, code){
            
            msgService.logMessage('menuService.getMenuItemById', "response: "+response+ "code: " + code);
                
            if(isFunction(err))
                err(response);
        });
        
    }
    
    this.updateMenuItem = function (data,succs, err)
    {
        $http.post('manager/'+data.id.rsId+'/menuitems/'+data.id.miId, data)
            
        .success(function(response){
            
            if(response.status == 401 || response.status == 500)
            {
                msgService.logMessage('menuService.updateMenuItem', response);
                
                if(isFunction(err))
                    err(response);
                return;
            }
            
            if(isFunction(succs))
            {
                msgService.logMessage('menuService.updateMenuItem', response);
                succs(response);
            }
            
        })
        .error(function(response, code){
            
            msgService.logMessage('menuService.updateMenuItem', "response: "+response+ "code: " + code);
                
             if(isFunction(err))
                err(response);
        });
        
    }
    
    this.deleteMenuItem = function (rsId,id,succs,err)
    {
        $http.delete('manager/'+rsId+'/menuitems/'+id)
        
        .success(function(){
            
            if(isFunction(succs))
                succs();
               
        })
        .error(function(response, code){
            
            msgService.logMessage('menuService.deleteMenuItem', "response: "+response+ "code: " + code);
                
            if(isFunction(err))
                err(response);
        });
        
    }
    
}]);