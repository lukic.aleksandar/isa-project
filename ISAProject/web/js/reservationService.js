isaProject.service('reservationService', ['$http', 'messageService', function($http, msgService){
    
    var isFunction = function (fun)
    {
        if(fun != undefined  && (typeof fun === 'function'))
            return true;
        return false;
    }
    
    this.createReservation = function(usrId,data, succs, err)
    {   
        data = JSON.parse(JSON.stringify(data));
        delete data.id.rsvSerial;
        $http.post('user/'+usrId+'/reservations/', data)
        
        .success(function(response){
            
            if(response.status == 401 || response.status == 500)
            {
                msgService.logMessage('reservationService.createReservation', response);
                
                if(isFunction(err))
                    err(response);
                return;
            }
            
            if(isFunction(succs))
            {
                msgService.logMessage('reservationService.createReservation', response);
                succs(response);
            }
            
        })
        .error(function(response, code){
            
            msgService.logMessage('reservationService.createReservation', "response: "+response+ "code: " + code);
                
            if(isFunction(err))
                err(response);
        });
        
    }
    
    
    this.getReservations = function(usrId, succs, err)
    {   
        $http.get('user/'+usrId+'/reservations/')
        
        .success(function(response){
            
            if(response.status == 401 || response.status == 500)
            {
                msgService.logMessage('reservationService.createReservation', response);
                
                if(isFunction(err))
                    err(response);
                return;
            }
            
            if(isFunction(succs))
            {
                msgService.logMessage('reservationService.createReservation', response);
                succs(response);
            }
            
        })
        .error(function(response, code){
            
            msgService.logMessage('reservationService.createReservation', "response: "+response+ "code: " + code);
                
            if(isFunction(err))
                err(response);
        });
        
    }
    
    
    
    this.updateReservation = function(usrId,data, succs, err)
    {   
        data = JSON.parse(JSON.stringify(data));
        delete data.id.rsvSerial;
        //OVO NIJE GOTOVO
        $http.post('user/'+usrId+'/reservations/'+data.id.rsId, data)
        
        .success(function(response){
            
            if(response.status == 401 || response.status == 500)
            {
                msgService.logMessage('reservationService.createReservation', response);
                
                if(isFunction(err))
                    err(response);
                return;
            }
            
            if(isFunction(succs))
            {
                msgService.logMessage('reservationService.createReservation', response);
                succs(response);
            }
            
        })
        .error(function(response, code){
            
            msgService.logMessage('reservationService.createReservation', "response: "+response+ "code: " + code);
                
            if(isFunction(err))
                err(response);
        });
        
    }
    
    
    
    
}]);