isaProject.service('loginService', ['$location', '$cookies','$http','$log', 
                   function($location, $cookies, $http, $log){
    
    this.login = function(data, success, error)
    {
        $http.post('auth/login', data)
        .success(function(response){
            
            if(response.usrId != undefined)
            {
                $cookies.putObject("user", response);
                if(success!=undefined)
                    success();
            }else{
                if(error!=undefined)
                    error(response, undefined);
            }
        })
        .error(function(response, code){
             if(error!=undefined)
                    error(response,code);
        });
    }
    
    this.logout = function(success, error)
    {
        $http.post('auth/logout','{}')
        .success(function(response){
            
            if(response.status == 200)
            {
                $cookies.remove("user");
                if(success != undefined)
                    success();
            }else{
                if(error!=undefined)
                    error(response, undefined);
            }
        })
        .error(function(response, code){
             if(error!=undefined)
                    error(response, code);
        });
    }
    
    this.register = function(data, success, error)
    {
        $http.post('auth/register', data)
        .success(function(response){
            
            if(response.status == 200)
            {
                if(success != undefined)
                    success();
            }else{
                if(error!=undefined)
                    error(response, undefined);
            }
        })
        .error(function(response, code){
            if(error!=undefined)
                    error(response, code);
        });
    }
    
    this.getLoggedUser = function()
    {   
        return $cookies.getObject('user');
    }
    
    this.redirectToLogin = function()
    {
        $location.path('/login');
    }
    
    this.hasPermission = function(permissions) 
    {
        var user = this.getLoggedUser();
        if(user == undefined)
        { 
            this.redirectToLogin();
            return false;
        }
        
        for (var i = 0; i < permissions.length; i++)
            if(user.usrRole === permissions[i])
                return true;
        return false;
    }
    
    this.stillLoggedIn = function(callback)
    {
        $http.get('auth/user')
        .success(function(response){
          
            if(response.status != 500 && response.status != 404)
            {   
                 $cookies.remove("user");
                 $cookies.putObject("user", response);
                 if(callback != undefined && (typeof callback === 'function'))    
                    callback();
            }else{
               $cookies.remove("user");
               $log.info("loginSevice.stillLogedIn:" + response);
               $location.path('/login');
            }
        })
        .error(function(response, code){
            $cookies.remove("user");
            $log.info("loginSevice.stillLogedIn:" + code);
            $location.path('/login');
        });
    }
    
}]);