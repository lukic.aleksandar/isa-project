isaProject.controller('adminController', ['$scope','$location','loginService','managerService','messageService','restaurantService', 
                                          function($scope,$location ,logService, mngrService, msgService, restService){
    
    $scope.managerInput = {
        usrId: "",
        usrEmail : "",
        usrPass : "",
        usrName: "",
        usrSurname: "",
        usrTown: "",
        usrAddress: "",
        restaurant: {
            rsId: ""
        }
    }
  
    $scope.restaurantInput = {
        rsId: "", 
        rsName: "",
        rsTown: "",
        rsAddress:"",
        rsDesc: "",
        rsAvggrade: ""
    }
    $scope.state = 'none';
    $scope.managers = [];
    $scope.restaurants = [];
    
    
    $scope.addManager = function ()
    {   
        
        if(!logService.hasPermission(['AD']))
            return;
        
        var succs = function(data){
             $scope.managers.push(data);   
             $scope.initManagerInput();
        }

        var err = function(){}
        mngrService.addManager($scope.managerInput, succs, err);
            
    }
    
    $scope.getManagers = function()
    {
        if(!logService.hasPermission(['AD']))
            return;
        
        var succs = function(data){
             $scope.managers = data;     
        }

        var err = function(){}

        mngrService.getManagers(succs, err);
    }
    
    $scope.getManagerById = function()
    {
        if(!logService.hasPermission(['AD']))
            return;
        
        var succs = function(data){
             $scope.managerTemp = data;
        }

        var err = function(){}

        mngrService.getManagerById($scope.managerInput.usrId, succs, err);
    }
    
    $scope.updateManager = function()
    {
        if(!logService.hasPermission(['AD']))
            return;
        
        var succs = function(data){
             
            for(var i = 0; i < $scope.managers.length; i++)
                if($scope.managers[i].usrId === data.usrId)
                    $scope.managers[i] = data
            $scope.state = 'none';
            $scope.initManagerInput();
        }

        var err = function(){}

        mngrService.updateManager($scope.managerInput, succs, err);
    }
    
    $scope.deleteManager = function(index)
    {
        if(!logService.hasPermission(['AD']))
            return;
        
        var succs = function(){
            $scope.managers.splice(index,1);
        }

        var err = function(){}

        mngrService.deleteManager($scope.managers[index].usrId, succs, err);
    }
    
    $scope.addRestaurant = function ()
    {   
        
        if(!logService.hasPermission(['AD']))
            return;
        
        var succs = function(data){
             $scope.restaurants.push(data);   
             $scope.initRestaurantInput();
        }

        var err = function(){}

        restService.addRestaurant($scope.restaurantInput, succs, err);
            
    }
    
    $scope.getRestaurants = function()
    {
        if(!logService.hasPermission(['AD']))
            return;
        
        var succs = function(data){
             $scope.restaurants = data; 
        }

        var err = function(){}

        restService.getRestaurants(succs, err);
    }
    
    $scope.getRestaurantsById = function()
    {
        if(!logService.hasPermission(['AD']))
            return;
        
        var succs = function(data){
             $scope.restaurantTemp = data;
        }

        var err = function(){}

        restService.getRestaurantById($scope.restaurantInput.rsId, succs, err);
    }
    
    $scope.updateRestaurant = function()
    {
        if(!logService.hasPermission(['AD']))
            return;
        
        var succs = function(data){
             
            for(var i = 0; i < $scope.restaurants.length; i++)
                if($scope.restaurants[i].rsId === data.rsId)
                    $scope.restaurants[i] = data
            $scope.state = 'none';
            $scope.initRestaurantInput();
        }

        var err = function(){}

        restService.updateRestaurant($scope.restaurantInput, succs, err);
    }
    
    $scope.deleteRestaurant = function(index)
    {
        if(!logService.hasPermission(['AD']))
            return;
        
        var succs = function(){
            $scope.restaurants.splice(index,1);
        }

        var err = function(){}

        restService.deleteRestaurant($scope.restaurants[index].rsId, succs, err);
    }
    
    
    $scope.prepareManagerUpdate = function (index)
    {
        var user = $scope.managers[index];
        
        for(var i = 0; i < $scope.restaurants.length; i++)
            if(user.restaurant.rsId === $scope.restaurants[i].rsId)
                $scope.managerInput.restaurant = $scope.restaurants[i];
        
        $scope.managerInput.usrAddress = user.usrAddress;
        $scope.managerInput.usrTown = user.usrTown;
        $scope.managerInput.usrEmail = user.usrEmail;
        $scope.managerInput.usrId = user.usrId;
        $scope.managerInput.usrName = user.usrName;
        $scope.managerInput.usrSurname = user.usrSurname;
        $scope.managerInput.usrPass = user.usrPass;
        
        $scope.state = 'update';
    }
    
    $scope.prepareRestaurantUpdate = function (index)
    {   
        var rest = $scope.restaurants[index];
       
        $scope.restaurantInput.rsId = rest.rsId; 
        $scope.restaurantInput.rsName = rest.rsName;
        $scope.restaurantInput.rsTown = rest.rsTown;
        $scope.restaurantInput.rsAddress = rest.rsAddress;
        $scope.restaurantInput.rsDesc = rest.rsDesc;
        
        $scope.state = 'update';
    }
    
    $scope.cancelUpdate = function()
    {
        $scope.state = 'none';
        $scope.initManagerInput();
        $scope.initRestaurantInput();
    }
    
    $scope.initManagerInput = function()
    {
        $scope.managerInput = {
            usrId: "",
            usrEmail : "",
            usrPass : "",
            usrName: "",
            usrSurname: "",
            usrTown: "",
            usrAddress: "",
            restaurant: {
                rsId: ""
            }
        }
    }
    
    $scope.initRestaurantInput = function()
    {
        $scope.restaurantInput = {
            rsId: "", 
            rsName: "",
            rsTown: "",
            rsAddress:"",
            rsDesc: "",
            rsAvggrade: ""
        }
    }
    
    var callback = function ()
    {   
        if(!logService.hasPermission(['AD']))
                return;

        if($location.path() === '/admin/managers')
        {    
            $scope.getRestaurants();
            $scope.getManagers();

        }else if ($location.path() === '/admin/restaurants')
            $scope.getRestaurants();
    }
    
    logService.stillLoggedIn(callback);
    
}]);
