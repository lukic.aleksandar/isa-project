var isaProject = angular.module('isaProject', ['ngRoute', 'ngCookies']);

isaProject.config(function($routeProvider){
    
    console.log($routeProvider);
    $routeProvider
        
        .when('/',{
            templateUrl: 'html/admin.html',
            controller: 'loginController'
        })
    
        .when('/admin/restaurants',{
            templateUrl: 'html/aRestaurants.html',
            controller: 'adminController'
        })
    
        .when('/admin/managers',{
        
            templateUrl: 'html/aManagers.html',
            controller: 'adminController'
        })
        .when('/afRegister',{
            templateUrl: 'html/afRegister.html'
            
        })
        .when('/login', {
            templateUrl: 'html/login.html',
            controller: 'loginController'
        })
        .when('/register', {
            templateUrl: 'html/register.html',
            controller: 'loginController'
        })
        .when('/logout', {
            templateUrl: 'html/login.html',
            controller: 'loginController'
        })
        .when('/afRegister', {
            templateUrl: 'html/afRegister.html',
            controller: 'loginController'
        })
        .when('/manager/menu', {
            templateUrl: 'html/mMenu.html',
            controller: 'managerController'
        })
        .when('/manager/restaurant', {
            templateUrl: 'html/mRestaurant.html',
            controller: 'managerController'
        })
        .when('/user/account', {
            templateUrl: 'html/uProfile.html',
            controller: 'userController'
        })
        .when('/user/home', {
            templateUrl: 'html/uHome.html',
            controller: 'gradeController'
        })
        .when('/user/friends', {
            templateUrl: 'html/uFriends.html',
            controller: 'userController'
        })
        .when('/restaurants', {
            templateUrl: 'html/uRestaurants.html',
            controller: 'reservationController'
        })
        .when('/manager/tabels', {
            templateUrl: 'html/mTabels.html',
            controller: 'managerController'
        })
        .when('/user/invite/conf/:token',{
            templateUrl: 'html/inviteConfirm.html',
            controller: 'invitecController'
        });
        
        
    
});
